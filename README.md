# Motto:  Something is better than nothing (SBTN) #

## Lecture 1
React = library																									
Virtual DOM																																																		
ReactDOM vs React																									
React.createElement vs. JSX																									
JSX			
Create react app																					
Component lifecycle																									
Fetch inside componentDidMount																									
setState																									
Passing props to child components																									
Conditionals																									
lists, map, keys																									
Homework																									
																									
## Lecture 2
Thinking in react (simple components)																									
Pure functions																									
PureComponents, Functional components																									
Passing function to setState																									
ShouldComponentUpdate																																																	
Homework
																									
## Lecture 3
React Router v4. API and how it works																									
How to write your own router																																																	
Homework
																									
## Lecture 4
CQRS																									
Flux																									
Example of working redux																									
Redux with pure javascript																									
Homework																									
													
## Lecture 5
Connecting React with Redux: Inconvenient way and Simple way											
Container vs presentational components (aka smart and dumb components)																									
Interception of actions using middlewares																									
How to use inputs with redux and react. Synthetic events																									
Selectors. Reselect library																									
Homework																									
																									
## Lecture 6
Async																									
Async in action creators																									
Redux thunk																									
Redux saga																									
Redux observable																									
Homework																									
																									
## Lecture 7
Structuring your application																									
Normalization																									
Immutable																									
Testing with jest																									
Homework																									
																									
## Lecture 8
Webpack																									
Entry/output																									
Loaders																									
Plugins																									
Webpacking react	
Hot module reload
Homework																																																
																									
## Lecture 9
GraphQL vs. REST																									
Queries																									
Mutations																									
Subscriptions																									
																									
## Lecture 10
Apollo framework																									
Writing schema	
Server side rendering
																									
																							