import React, { Component } from 'react';
import './App.css';
import HeroesList from './components/HeroesList';
import HeroCard from './components/HeroCard';

class App extends Component {
  constructor() {
    super();

    this.state = {
      heroes: [],
      currentHero: {},
    };
  }

  onClick = (heroName) => {
    this.setState({
      currentHero: this.state.heroes.find(({ name }) => name === heroName),
    });
  };

  componentDidMount() {
    fetch('//swapi.co/api/people')
      .then(response => response.json())
      .then(data => data.results)
      .then(heroes => this.setState({ heroes, currentHero: heroes[0] }));
  }


  render() {
    return (
      <div className="container">
        <HeroesList heroes={this.state.heroes} selected={this.state.currentHero} onClick={this.onClick}/>
        <HeroCard hero={this.state.currentHero}/>
      </div>
    );
  }
}

export default App;
