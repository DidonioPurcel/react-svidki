import React, { Component } from 'react';

class Hero extends Component {
  onClick = () => {
    this.props.onClick(this.props.hero.name);
  };

  render() { //note about frequent call of render function and as the result memory consumption in case of creation of functions inside render
    return (
      <a onClick={this.onClick}>{this.props.hero.name}</a>
    );
  }
}

export default Hero;
