import React, { Component } from 'react';

class HeroCard extends Component {

  render() { //note about frequent call of render function and as the result memory consumption in case of creation of functions inside render
    return (
      <div className="col">
        <div className="label">
          <span>Name: </span>
          <span>{this.props.hero.name}</span>
        </div>
        <div className="label">
          <span>Height: </span>
          <span>{this.props.hero.height}</span>
        </div>
        <div className="label">
          <span>Mass: </span>
          <span>{this.props.hero.mass}</span>
        </div>
        <div className="label">
          <span>Birth: </span>
          <span>{this.props.hero.birth_year}</span>
        </div>
      </div>
    );
  }
}

export default HeroCard;
