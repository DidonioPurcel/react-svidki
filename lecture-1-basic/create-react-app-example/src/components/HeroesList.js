import React, { Component } from 'react';
import Hero from './Hero';
import PropTypes from 'prop-types';

class HeroesList extends Component {
  static propTypes = {
    selected: PropTypes.bool.isRequired,
    onClick: PropTypes.func.isRequired,
    heroes: PropTypes.array.isRequired
  };

  render() {
    return (
      <nav className="col">
        {
          this.props.heroes.map(hero => (
            <div className="hero">
              <span>{hero === this.props.selected ? '>' : ''}</span>
              <Hero key={hero.name} hero={hero} onClick={this.props.onClick}/>
            </div>
          ))
        }
      </nav>
    );
  }
}

export default HeroesList;
