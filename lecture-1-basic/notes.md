## Урок 1																									
React = library
  Not V in MVC :)		
																						
###Virtual DOM
  Tree of objects (not dom elements) describing how real dom should look like
  Change in model leads to building of new virtual dom 
    => old one is compared to new one 
    => changed are applied to real dom
																																																	
###ReactDOM vs React, 
   React dom - glue between virtual dom objects (elements) and real dom tree
																							
###JSX																								
  JSX	- just extension of js.
  is being transpiled into function call depending on library (React.createElement) - https://mithril.js.org/,
  React.createElement 

###Create react app	
  cli tool,
  you can eject configs if you need to change them
  																				
###Component lifecycle
  4 types of component lifecycle events
  Fetch inside componentDidMount						
  
###Component's state (this.state)
  setting state
  rendering due to changing state
																									
###Props (this.props)
  Passing props to child components
  Passing functions as inversion of control
  Default props
  Props validation (prop types)
																										
###Js in jsx
  expressions returning children -  Lists, Array.prototype.map, map keys
  attribute expressions
  usage of spread operator in attribute expressions


###Homework	

Create react app using create-react-app tool.
1) fetch list of octocat's repos (https://api.github.com/orgs/octokit/repos).
2) Display a list of repos' names.
3) By clicking on a single repo show detailed information about this repo. 
  (full_name, html_url, description, language, owner.login, owner.avatar_url)


#Additional reading:
* (What If React Was Really Only The V in MVC?) https://medium.com/javascript-inside/what-if-react-was-really-only-the-v-in-mvc-5854fd6f601c
* (How to write your own Virtual DOM) https://medium.com/@deathmood/how-to-write-your-own-virtual-dom-ee74acc13060
* (Reconciliation) https://facebook.github.io/react/docs/reconciliation.html
* (A look inside React Fiber - how work will get done) http://makersden.io/blog/look-inside-fiber/?utm_campaign=Fullstack%2BReact&utm_medium=email&utm_source=Fullstack_React_70
* (Where to Fetch Data: componentWillMount vs componentDidMount) https://daveceddia.com/where-fetch-data-componentwillmount-vs-componentdidmount/
* (The component lifecycle) https://facebook.github.io/react/docs/react-component.html#the-component-lifecycle
* (Typechecking with propTypes) https://facebook.github.io/react/docs/typechecking-with-proptypes.html
* (React internals, part 1) http://www.mattgreer.org/articles/react-internals-part-one-basic-rendering