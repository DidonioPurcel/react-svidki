import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import gql from 'graphql-tag';

const client = new ApolloClient({
  link: new HttpLink({ uri: '/graphql' }),
  cache: new InMemoryCache()
});

client
  .query({
    query: gql(`
      query {
        bands {
          id
          name
          origin
          years
        }
      }
    `)
  })
  .then(response => console.log(response));
