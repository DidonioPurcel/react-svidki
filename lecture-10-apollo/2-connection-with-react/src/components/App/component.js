import React from 'react';
import PropTypes from 'prop-types';

const App = ({ bands }) => {
  if (!bands) { return null; }
  return (
    <div>
      {bands.map(band => <div key={band.id}>{band.name}</div>)}
    </div>
  );
};

App.propTypes = {
  bands: PropTypes.array
};

export default App;
