import { graphql } from 'react-apollo';
import AppComponent from './component';
import bandsQuery from './bandsQuery.graphql';

export default graphql(
  bandsQuery,
  {
    props: (response) => {
      console.log(response);

      return {
        bands: response.data.bands
      };
    }
  }
)(AppComponent);
