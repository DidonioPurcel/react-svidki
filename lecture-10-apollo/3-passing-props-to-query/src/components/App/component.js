import React from 'react';
import PropTypes from 'prop-types';

const App = ({ band }) => {
  if (!band) { return null; }
  return (
    <div>
      <p>{band.name}</p>
      <p>{band.origin}</p>
      <p>{band.years}</p>
      <p>{band.genre}</p>
      <p>Members: {band.members.map(member => <span key={member.id}>{member.name}, </span>)}</p>
      <p>Albums: {band.albums.map(album => <span key={album.id}>{album.name}, </span>)}</p>
    </div>
  );
};

App.propTypes = {
  band: PropTypes.object
};

export default App;
