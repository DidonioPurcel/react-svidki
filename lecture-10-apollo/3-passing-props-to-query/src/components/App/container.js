import { graphql } from 'react-apollo';
import AppComponent from './component';
import bandQuery from './bandQuery.graphql';

const mapGraphqlToProps = ({ data: { band } }) => ({ band });

export default graphql(
  bandQuery,
  {
    options: () => ({ variables: { id: '59fad04b9c040e4bc1fcd608' } }),
    props: mapGraphqlToProps,
  }
)(AppComponent);
