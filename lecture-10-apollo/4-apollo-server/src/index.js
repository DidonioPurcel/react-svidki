import { createServer } from 'http';
import { SubscriptionServer } from 'subscriptions-transport-ws';
import { graphqlExpress, graphiqlExpress } from 'apollo-server-express';
import { execute, subscribe } from 'graphql';
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';

import schema from './schema';

const db = require('monk')('localhost/bands');

const port = 3000;

const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/graphql', graphqlExpress({
  schema
}));

app.use('/graphiql', graphiqlExpress({
  endpointURL: '/graphql',
  subscriptionsEndpoint: `ws://localhost:${port}/subscriptions`
}));

const server = createServer(app);

server.listen(port, () => {
  console.log(`Apollo Server is now running on http://localhost:${port}`);

  // eslint-disable-next-line no-new
  new SubscriptionServer({
    execute,
    subscribe,
    schema
  }, {
    server,
    path: '/subscriptions'
  });
});

db.get('bands').find().then((bandsList) => {
  if (bandsList.length > 0) {
    return false;
  }

  const bands = [
    {
      id: 1,
      name: 'Jungle',
      origin: 'London, England',
      years: '2013-present',
      genre: 'Neo-soul',
      members: [1, 2, 3, 4, 5, 6, 7],
      albums: [1]
    },
    {
      id: 2,
      name: 'Cradle of Filth',
      origin: 'Suffolk, England',
      years: '1991-present',
      genre: 'extreme metal',
      members: [8, 9, 10, 11, 12, 13],
      albums: [2, 3, 4]
    },
    {
      id: 3,
      name: 'Alt-J',
      origin: 'Leeds, England',
      years: '2007-present',
      genre: 'indie rock',
      members: [14, 15, 16],
      albums: [5, 6, 7]
    }
  ];

  bands.forEach(band => db.get('bands').insert(band));
  return true;
});
