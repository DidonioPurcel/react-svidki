import { makeExecutableSchema } from 'graphql-tools';
import { PubSub } from 'graphql-subscriptions';

const db = require('monk')('localhost/bands');

const pubsub = new PubSub();
const BAND_ADDED = 'bandAdded';

const schema = [`
schema {
  query: Query
  mutation: Mutation
  subscription: Subscription
}

type Query {
  bands: [Band]
}

type Mutation {
  addBand(input: BandInput): Band
}

type Subscription {
  bandAdded: Band
}

type Band {
  id: ID!
  name: String!
  origin: String
  years: String
  genre: String
  members: [Int]
  albums: [Int]
}

input BandInput {
  name: String!
  origin: String
  years: String
  genre: String
}
`];

const resolvers = {
  Query: {
    bands: () => db.get('bands').find(),
  },
  Band: {
    id: root => root._id,
    name: root => root.name,
    origin: root => root.origin,
    years: root => root.years,
    genre: root => root.genre,
    members: root => root.members,
    albums: root => root.albums
  },
  Mutation: {
    addBand(_, { input }) {
      return db.get('bands').insert(input).then((result) => {
        pubsub.publish(BAND_ADDED, { bandAdded: input });
        return result;
      });
    }
  },
  Subscription: {
    bandAdded: {
      subscribe: () => pubsub.asyncIterator(BAND_ADDED),
    }
  }
};

export default makeExecutableSchema({
  typeDefs: [...schema],
  resolvers: resolvers
});
