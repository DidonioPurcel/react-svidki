import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Band from '../Band';

class App extends Component {
  componentWillMount() {
    this.props.subscribeToNewBands();
  }

  render() {
    const { bands } = this.props;

    if (!bands) { return null; }
    return (
      <div>
        {bands.map(band => <Band key={band.id} band={band} />)}
      </div>
    );
  }
}

App.propTypes = {
  bands: PropTypes.array,
  subscribeToNewBands: PropTypes.func
};

export default App;
