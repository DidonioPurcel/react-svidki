import { graphql } from 'react-apollo';
import AppComponent from './component';
import bandsQuery from './bandsQuery.graphql';
import bandAddedSubscription from './bandAddedSubscription.graphql';

export default graphql(
  bandsQuery,
  {
    props: props => ({
      bands: props.data.bands,
      subscribeToNewBands: () =>
        props.data.subscribeToMore({
          document: bandAddedSubscription,
          updateQuery: (prev, { subscriptionData }) => {
            if (!subscriptionData.bandAdded) {
              return prev;
            }
            return Object.assign({}, prev, {
              bands: [subscriptionData.bandAdded, ...prev.bands]
            });
          }
        })
    }),
  }
)(AppComponent);
