import React from 'react';
import PropTypes from 'prop-types';
import Band from '../Band';

const App = ({ bands }) => {
  if (!bands) { return null; }
  return (
    <div>
      {bands.map(band => <Band key={band.id} band={band} />)}
    </div>
  );
};

App.propTypes = {
  bands: PropTypes.array
};

export default App;
