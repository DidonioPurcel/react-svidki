import { graphql } from 'react-apollo';
import AppComponent from './component';
import bandsQuery from './bandsQuery.graphql';

export default graphql(
  bandsQuery,
  {
    props: props => ({
      bands: props.data.bands
    }),
  }
)(AppComponent);
