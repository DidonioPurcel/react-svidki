import React from 'react';
import PropTypes from 'prop-types';

const Band = ({ band }) => (
  <div>
    <p>{band.name}</p>
    <p>{band.origin}</p>
    <p>{band.years}</p>
    <p>{band.genre}</p>
    <br />
    <br />
  </div>
);

Band.propTypes = {
  band: PropTypes.object
};

export default Band;
