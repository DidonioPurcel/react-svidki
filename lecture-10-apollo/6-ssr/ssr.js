import express from 'express';
import compression from 'compression';
import React from 'react';
import { ApolloProvider, renderToStringWithData } from 'react-apollo';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import fetch from 'node-fetch';

import App from './src/components/App';
import html from './public/index.html';

const app = express();
const port = process.env.PORT || 3010;

app.use(compression());

app.get('*', (req, res) => {
  const client = new ApolloClient({
    ssrMode: true,
    link: createHttpLink({ uri: 'http://localhost:3000/graphql', fetch }),
    cache: new InMemoryCache()
  });

  const clientApp = (
    <ApolloProvider client={client}>
      <App />
    </ApolloProvider>
  );

  renderToStringWithData(clientApp).then((content) => {
    const document = html.replace(/<div id="root"><\/div>/, `<div id="root">${content}</div>`);
    res.send(document);
  });
});

app.listen(port, () => console.log('Server side rendering running at: ', port));
