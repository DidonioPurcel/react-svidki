const webpack = require('webpack');
const { resolve } = require('path');
const nodeExternals = require('webpack-node-externals');
const Config = require('webpack-config').default;
const HtmlWebpackPlugin = require('html-webpack-plugin');
const precss = require('precss');
const autoprefixer = require('autoprefixer');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const OfflinePlugin = require('offline-plugin');

const config = new Config();
const isDev = process.env.NODE_ENV === 'development';
const isProd = process.env.NODE_ENV === 'production';
const isSSR = process.env.SIDE === 'server';
const isClient = process.env.SIDE === 'client';

/**
 * Common
 */
config.merge({
  entry: resolve(__dirname, 'src/index.js'),
  output: {
    filename: 'main.[hash].js',
    path: resolve(__dirname, 'dist')
  },
  context: resolve(__dirname),
  devtool: 'source-map',
  bail: isProd,
  resolve: {
    extensions: ['.css', '.js', '.jsx', '.json']
  },
  module: {
    rules: [
      {
        test: /\.(graphql|gql)$/,
        exclude: /node_modules/,
        loader: 'graphql-tag/loader'
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: [
          'babel-loader',
          'eslint-loader'
        ]
      },
      {
        test: /\.(css|scss)$/,
        use: ExtractTextPlugin.extract({
          use: [
            {
              loader: 'css-loader',
              options: {
                sourceMaps: true,
                modules: true,
                importLoaders: 1,
                localIdentName: '[local]__[hash:base64:5]'
              }
            },
            {
              loader: 'postcss-loader',
              options: {
                sourceMaps: true,
                plugins: () => [
                  precss,
                  autoprefixer({
                    browsers: [
                      'last 2 versions',
                      'IE >= 10',
                      'safari >= 8'
                    ]
                  })
                ]
              }
            },
            {
              loader: 'sass-loader'
            }
          ]
        })
      },
      { test: /\.json$/, loader: 'json-loader' },
      {
        test: /\.html$/,
        loader: 'html-loader',
        options: {
          minimize: true,
          removeAttributeQuotes: false
        }
      },
      { test: /\.(png|jpg|jpeg|gif|svg|woff|woff2)$/, loader: 'url-loader' },
    ]
  },
  plugins: [
    new webpack.EnvironmentPlugin({
      NODE_ENV: process.env.NODE_ENV,
    }),
    new webpack.NamedModulesPlugin(),
    new ExtractTextPlugin({
      filename: '[name].[chunkhash].css',
      allChunks: true,
    })
  ]
});


/**
 * Development
 */
if (isDev) {
  config.merge({
    output: {
      pathinfo: true
    },
    devServer: {
      contentBase: resolve(__dirname, 'public'),
      publicPath: '/',
      hot: true,
      inline: true,
      port: 8085,
      historyApiFallback: true,
      stats: {
        colors: true,
        chunks: false,
        'errors-only': true
      },
      // noInfo: true,
      // quiet: true
    },
    plugins: [
      new webpack.HotModuleReplacementPlugin()
    ]
  });
}

/**
 * Production
 */
if (isProd) {
  config.merge({
    output: {
      filename: 'main.[chunkhash].js'
    },
    devServer: {
      compress: true
    },
    plugins: [
      new webpack.optimize.OccurrenceOrderPlugin(),
      new webpack.LoaderOptionsPlugin({
        minimize: true,
        debug: false
      }),
      new webpack.optimize.UglifyJsPlugin({
        sourceMap: true,
        compress: {
          warnings: false,
          screw_ie8: true,
          conditionals: true,
          unused: true,
          comparisons: true,
          sequences: true,
          dead_code: true,
          evaluate: true,
          if_return: true,
          join_vars: true
        },
        output: {
          comments: false,
          beautify: false
        },
      })
    ]
  });
}

/**
 * Client development
 */
if (isClient) {
  config.merge({
    plugins: [
      new HtmlWebpackPlugin({
        template: resolve(__dirname, 'public/index.html'),
        inject: 'body'
      })
    ]
  });
}

/**
 * Client production
 */
if (isProd && isClient) {
  config.merge({
    plugins: [
      new CleanWebpackPlugin(['dist'], {
        root: __dirname,
        verbose: false,
        dry: false
      }),
      new CopyWebpackPlugin([
        { from: 'public' }
      ]),
      new OfflinePlugin({
        publicPath: '/',
        caches: {
          main: [
            'main.*.js',
            'main.*.css',
            'vendor.*.js'
          ],
          additional: [
            ':externals'
          ],
          optional: [
            ':rest:'
          ]
        },
        externals: [
          '/'
        ],
        ServiceWorker: {
          navigateFallbackURL: '/'
        },
        AppCache: {
          FALLBACK: {
            '/': '/offline-page.html'
          }
        }
      })
    ]
  });
}

/**
 * Server side rendering
 */
if (isSSR) {
  config.merge({
    target: 'node',
    externals: [nodeExternals()],
    entry: resolve(__dirname, 'ssr.js'),
    devtool: 'source-map',
    output: {
      path: resolve(__dirname, 'dist'),
      filename: 'ssr.js',
      publicPath: 'dist/',
      libraryTarget: 'commonjs2'
    }
  });
}

module.exports = config;
