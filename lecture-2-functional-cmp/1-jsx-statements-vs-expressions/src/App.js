import React, { Component } from 'react';
import './App.css';
import images from './imageLinks';

class App extends Component {

  render() {
    const fanOf = 'superman';

    if (fanOf === 'batman') {
      return <img src="http://vignette1.wikia.nocookie.net/batman/images/c/c8/Batman_%2766_-_Adam_West_as_Batman_2.jpg/revision/latest?cb=20140731220401" alt=""/>;
    } else {
      return <img src="https://static.comicvine.com/uploads/original/10/102481/3176740-2052305307-tumbl.jpg" alt=""/>
    }
  }
  //
  // render() {
  //   const fanOf = 'batman';
  //
  //   switch (fanOf) {
  //     case 'batman':
  //       return <img src={images.batman} alt=""/>;
  //     case 'superman':
  //           return <img src={images.superman} alt=""/>;
  //     default:
  //       return <img src={images.trueHero} alt=""/>;
  //   }
  // }

  // render() {
  //   const fanOf = 'batman';
  //
  //   return (
  //     <div>
  //       {
  //           fanOf === 'batman'
  //               ? <img src={images.batman} alt=""/>
  //               : <img src={images.trueHero} alt=""/>
  //       }
  //     </div>
  //   );
  // }
}

export default App;
