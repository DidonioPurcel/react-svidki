import React, {Component} from 'react';
import './App.css';

class App extends Component {
    onClick () {
        console.log(this.props.ababagalamaga);
    }

    render () {
        return (
            <span>
                <button onClick={this.onClick}>Press me hard</button>
            </span>
        );
    }
}

App.defaultProps = {
    ababagalamaga: 'A-B-A-B-A-G-A-L-A-M-A-G-A'
};

export default App;
