import React, {Component} from 'react';
import './App.css';

class SetStateComponent extends Component {
    state = {
        counter: 1
    };

    componentDidMount () {
        setInterval(this.increment, 1000);
    }

    increment = () => {
        this.setState({counter: state.counter + this.props.increment});
        this.setState({counter: state.counter + this.props.increment});
        this.setState({counter: state.counter + this.props.increment});
    };

    render () {
        return (
            <span>{this.state.counter}</span>
        )
    }
}

class App extends Component {
    render () {
        return (
            <div>
                <SetStateComponent increment={1}/>
                {/*<MyContainer></MyContainer>*/}
            </div>
        );
    }
}


class MyContainer extends Component {
    render () {
        return (
            <div>
                I am container!
            </div>
        );
    }
}


/*class App extends Component {
    render() {
        return (
            <MyComponent data={this.onClick}>
                {
                    function (authenticated) {
                        return authenticated
                            ? <span>{this.props.name}</span>
                            : <span>You are not logged in</span>
                    }
                }
            </MyComponent>
        );
    }
}*/

export default App;
