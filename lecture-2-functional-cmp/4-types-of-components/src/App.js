import React, { Component } from 'react';
import './App.css';
import FunctionalComponent from './components/FunctionalComponent';
import hocData from './components/HOCData';
import hocWaiting from './components/HOCWaiting';
import { SimplestComponent, hoc } from "./components/hoc-simple";

const DataLoadingWaitingComponent = hocData('data', hocWaiting(5000, FunctionalComponent));

const SomethingClassComponent = hoc(SimplestComponent, {className: 'something'});

class App extends Component {
    render() {
        return (
            <div>
                {/*<ClassComponent></ClassComponent>*/}
                {/*<br/><br/>*/}
                {/*<OurPureComponent></OurPureComponent>*/}
                {/*<br/><br/>*/}
                {/*<FunctionalComponent></FunctionalComponent>*/}
                {/*<DataLoadingWaitingComponent/>*/}
                <SomethingClassComponent/>
                <SomethingClassComponent/>
                <SomethingClassComponent/>
            </div>
        )
    }
}

export default App;
