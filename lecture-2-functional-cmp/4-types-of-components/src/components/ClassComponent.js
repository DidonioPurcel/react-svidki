import React, {Component} from 'react';

class ClassComponent extends Component {
    constructor () {
        super();

        this.state = {
            familyRole: 'father'
        };

        setInterval(() => {
            this.setState({
                familyRole: 'mother'
            });
        }, 2000);
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextState.familyRole !== this.state.familyRole;
    }

    render () {
        console.log('I am not pure, and I am rerererendering!');

        return (
            <span>
                ClassComponent says: I am your
                <span className="family-role"> {this.state.familyRole}</span>, Pure Component!
            </span>
        );
    };

}

export default ClassComponent;