import React, { Component } from 'react';


// class FunctionalComponent extends Component {
//     render() {
//         <div>
//             <span>FunctionalComponent says: I am better than you are. Just wait to know why. </span>
//             <span>{JSON.stringify(this.props)}</span>
//         </div>
//     }
// }

// function FunctionalComponent (props) {
//     return (
//         <div>
//             <span>FunctionalComponent says: I am better than you are. Just wait to know why. </span>
//             <span>{JSON.stringify(props)}</span>
//         </div>
//     );
// }

const style = {'border': 'green'};


const FunctionalComponent = (props) => (
    <span style={style}>FunctionalComponent says: I am better than you are because: </span>
);

export default FunctionalComponent;