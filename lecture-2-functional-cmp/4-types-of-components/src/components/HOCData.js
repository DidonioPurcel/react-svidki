import React, {Component} from 'react';

function load () {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve([
                {
                    name: 'Luke',
                },
                {
                    name: 'Anakin',
                }
            ]);
        }, 1000);
    });
}

function DataComponent (propName, component) {
    return class extends Component {
        constructor (...args) {
            super(...args);
            this.state = {
                data: null
            };
        }

        componentDidMount () {
            load().then((data) => {
                this.setState({
                    data
                });
            });
        }

        render () {
            return this.state.data
                ? React.createElement(component, {[propName]: this.state.data})
                : <span>Loading data...</span>;
        }
    };
}

export default DataComponent;