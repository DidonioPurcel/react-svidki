import React, {Component} from 'react';

function WaitingComponent (time, component) {
    return class extends Component {
        constructor (...args) {
            super(...args);

            this.state = {
                show: false
            };

            setTimeout(() => {
                this.setState({
                    show: true
                });
            }, time);
        }

        render () {
            return this.state.show
                ? React.createElement(component, this.props)
                : <span>Please wait...</span>;
        }
    };
}

export default WaitingComponent;