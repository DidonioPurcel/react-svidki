import React, {PureComponent} from 'react';


//https://github.com/facebook/react/blob/9921e572722c0fa9d3cda65317f6de60f5d0919f/src/renderers/shared/fiber/ReactFiberClassComponent.js#L166
class OurPureComponent extends PureComponent {
    constructor () {
        super();

        this.state = {
            familyRole: 'son'
        };

        setInterval(() => {
            this.setState({
                familyRole: 'daughter'
            });
        }, 2000);
    }

    render () {
        console.log('I am pure, I will be rendered just once with the same state, bi***es.');

        return (
            <span>
                ClassComponent says: I am your
                <span className="family-role"> {this.state.familyRole}</span>, Component!
            </span>
        );
    };
}

export default OurPureComponent;