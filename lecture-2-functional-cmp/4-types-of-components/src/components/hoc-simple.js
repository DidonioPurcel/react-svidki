import React from 'react';

export const hoc = (Component, props) => {
    return () => {
        return <Component {...props} enhanced={true}/>
    };
};

export const SimplestComponent = (props) => {
    return (
        <pre>
            {JSON.stringify(props)}
        </pre>
    );
};