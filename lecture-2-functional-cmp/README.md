## Lesson 2

### jsx
* statements vs expressions

### props
* passing them in a right way
* do not generate new functions/objects/arrays in render function (! if it can be avoided)

### Passing children properly
* children are passed implicitly through props
* you can pass anything as children

### Passing function to setState
* setState is definitely asynchronous in version 16+;

### Types of components
* Class Components vs. Functional Components vs. Pure Components
* ShouldComponentUpdate
1) class component to functional component
2) class component + shouldComponentUpdate => preventing of unnecessary re-renderings
3) class component + shallow shouldCompnentUpdate check => Pure Component

##### Why usage of functional components is the best way
React team emphasized that components should be regarded as view function of props

### HOC (Higher-Order components)

### Hometask
Rewrite your app from homework 1 with higher order components.
* Components, which loads your data has to be wrapped with hoc. 
  On data load it has to show loading message. 
  Also it has to show 'no data' message when no data is received.
  When the data is received it has to render your component.
* Rewrite all your components as functional components where it is possible.
  Rewrite all your components as pure components where you can't use functional components (if it is possible)
* Try to use all best techniques that were shown in lecture.
* Don't forget to have fun)


### Additional readings:
* (Turn React Components Inside Out with Functional Programming) https://www.bignerdranch.com/blog/destroy-all-classes-turn-react-components-inside-out-with-functional-programming/?utm_source=reactnl&utm_medium=email
* (Functional setState) https://medium.freecodecamp.org/functional-setstate-is-the-future-of-react-374f30401b6b
* (When to use pure components) https://habrahabr.ru/company/redmadrobot/blog/318222/
* (Recomposing your react app (About High-order components, ~first 20 minutes)) https://www.youtube.com/watch?v=zD_judE-bXk

