### React Router v4. API and how it works	

### How to write your own router

### Hometask
* Basic: Add routing to your octokit app
* Advanced: Finish your own react router with adding Switch component.
* Much advanced: Implement higher-order component 'withRouter'. 
It has to listen to route changes and pass them to child component.
* Have fun)

### Additional readings:
* (React Router v4) https://www.youtube.com/watch?v=Vur2dAFZ4GE
* (Learn once, route everywhere) https://www.youtube.com/watch?v=Mf0Fy8iHp8k
* (Build your own React router v4) https://tylermcginnis.com/build-your-own-react-router-v4/
