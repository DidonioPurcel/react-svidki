import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {match as checkMatch} from './match';

class Route extends Component {
    static propTypes = {
        exact: PropTypes.bool,
        path: PropTypes.string,
        render: PropTypes.func,
    };

    state = {
        match: this.computeMatch()
    };

    updateMatch = () => {
        this.setState({
            match: this.computeMatch(),
        });
    };

    computeMatch () {
        const {path, exact} = this.props;

        const match = checkMatch(window.location.hash, path, exact);

        return match || null;
    }

    componentDidMount () {
        window.addEventListener('hashchange', this.updateMatch);
    }

    componentWillUnmount () {
        window.removeEventListener('hashchange', this.updateMatch);
    }

    render () {
        const {match} = this.state;

        if (match) {
            const {render} = this.props;

            return render
                ? render({match})
                : this.props.children;
        }

        return null;
    }
}

export {Route};
