import pathToRegexp from 'path-to-regexp';

export const match = (hash, path, exact = false) => {
    const keys = [];
    const regexp = pathToRegexp(path, keys, {end: exact});

    const matches = regexp.exec(hash.substr(1));

    if (matches) {
        return {
            url: matches[0],
            isExact: exact,
            params: keys.reduce((acc, item, index) => {
                acc[keys[index].name] = matches[index + 1];
                return acc;
            }, {})
        };
    }
};


console.log(match('#/first', '/first'), true);
console.log(match('#/first', '/first', true), true);
console.log(match('#/se', '/first'), false);
console.log(match('#/first/second', '/first/second'), true);
console.log(match('#/first/second', '/first', true), false);
console.log(pathToRegexp('#/first/:a/second/:b'));
const keys = [];
console.log(pathToRegexp('#/first/:a/second/:b', keys, {end: false}));
console.log(keys);
console.log(match('#/first/fff/second/ddd', '/first/:a/second/:b'), ['/first/fff/second/ddd', 'fff', 'ddd'])
