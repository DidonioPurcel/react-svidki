import React from 'react';
import PropTypes from 'prop-types';

const Link = ({children, to}) => (
    <a href={`/#${to}`}>{children}</a>
);

Link.propTypes = {
    to: PropTypes.string
};

export {Link};