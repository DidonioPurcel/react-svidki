import React, {Component} from 'react';
import PropTypes from 'prop-types';

export class Redirect extends Component {
    static propTypes = {
        to: PropTypes.string.isRequired,
    };
}