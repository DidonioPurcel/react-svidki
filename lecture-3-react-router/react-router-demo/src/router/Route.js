import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {match as checkMatch} from './match';

class Route extends Component {
    static propTypes = {
        path: PropTypes.string,
        exact: PropTypes.bool,
        component: PropTypes.func
    };

    state = {
        match: null
    };

    computeMatch () {
        return checkMatch(window.location.hash, this.props.path, this.props.exact);
    }

    updateMatch = () => {
        this.setState({
            match: this.computeMatch()
        });
    };

    componentDidMount () {
        window.addEventListener('hashchange', this.updateMatch);
    }

    componentWillUnmount () {
        window.removeEventListener('hashchange', this.updateMatch);
    }

    render () {
        const {match} = this.state;

        return match
            ? this.props.component({match})
            : null;
    }
}

export {Route};
