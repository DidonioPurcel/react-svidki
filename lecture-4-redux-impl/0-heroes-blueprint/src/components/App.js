import React from 'react';
import Heroes from './Heroes';
import SelectedHero from './SelectedHero';

const App = () => (
  <div className="app">
    <Heroes />
    <SelectedHero />
  </div>
);

export default App;
