import React from 'react';

const handleClick = (ev, hero) => {
  ev.preventDefault();
};

const Hero = ({ hero }) =>
  <a
    className="hero"
    onClick={(ev) => handleClick(ev, hero)}
  >
    {hero.name}
  </a>;

export default Hero;
