import React, { Component } from 'react';
import Hero from './Hero';

class Heroes extends Component {
  state = {};

  componentWillMount() {
    fetch('https://swapi.co/api/people')
      .then(res => res.json())
      .then(data => data.results)
      .then(heroes => this.setState({ heroes }));
  }

  render() {
    if (!this.state.heroes) { return null; }

    return (
      <nav className="col">
        {this.state.heroes.map(hero => <Hero key={hero.name} hero={hero} />)}
      </nav>
    );
  }
}

export default Heroes;
