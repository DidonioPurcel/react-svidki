import React, { Component } from 'react';

class SelectedHero extends Component {
  state = {};

  componentWillMount() {

  }

  render() {
    if (!this.state.hero) { return null; }

    return (
      <div className="hero">{this.state.hero.name}</div>
    );
  }
}

export default SelectedHero;
