import React, { Component } from 'react';
import Hero from './Hero';

class Heroes extends Component {
  state = {};

  componentWillMount() {
    const { dispatch, subscribe, getState } = window.store;

    fetch('//swapi.co/api/people')
      .then(res => res.json())
      .then(data => data.results)
      .then(heroes => dispatch({ type: 'SAVE_HEROES', heroes }));

    subscribe(() => {
      this.setState(state => ({ ...state, heroes: getState().heroes}))
    });
  }

  render() {
    if (!this.state.heroes) { return null; }

    return (
      <nav>
        {this.state.heroes.map((hero, index) => <Hero key={hero.name} index={index} hero={hero} />)}
      </nav>
    );
  }
}

export default Heroes;
