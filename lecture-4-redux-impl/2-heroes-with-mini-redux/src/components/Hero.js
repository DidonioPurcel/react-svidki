import React from 'react';

const handleClick = (ev, index) => {
  ev.preventDefault();

  window.store.dispatch({
    type: 'SELECT_HERO',
    selectedHeroIndex: index
  });
};

const Hero = ({ hero, index }) => <a onClick={(ev) => handleClick(ev, index)}>{hero.name}</a>;

export default Hero;
