import React, { Component } from 'react';

class SelectedHero extends Component {
  state = {};

  componentWillMount() {
    const { subscribe, getState } = window.store;

    subscribe(() => {
      const { heroes, selectedHeroIndex } = getState();
      this.setState(state => ({ ...state, hero: heroes[selectedHeroIndex] }))
    });
  }

  render() {
    if (!this.state.hero) { return null; }

    return (
      <div>{this.state.hero.name}</div>
    );
  }
}

export default SelectedHero;
