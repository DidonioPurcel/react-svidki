import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/App';
import { createStore } from './redux';

const initialState = {
  heroes: [],
  selectedHeroIndex: 0
};

const reducer = (state = {}, action) => {
  switch (action.type) {
    case 'SAVE_HEROES':
      return {
        ...state,
        heroes: [
          ...state.heroes,
          ...action.heroes
        ],
      };
    case 'SELECT_HERO':
      return {
        ...state,
        selectedHeroIndex: action.selectedHeroIndex
      };
    default:
      return state;
  }
};

window.store = createStore(reducer, initialState);

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
