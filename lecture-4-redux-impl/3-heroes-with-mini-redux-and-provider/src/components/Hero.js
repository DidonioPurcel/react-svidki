import React from 'react';
import PropTypes from 'prop-types';

const handleClick = (ev, index, store) => {
  ev.preventDefault();

  store.dispatch({
    type: 'SELECT_HERO',
    selectedHeroIndex: index
  });
};

const Hero = ({ hero, index }, { store }) => <a onClick={(ev) => handleClick(ev, index, store)}>{hero.name}</a>;

Hero.contextTypes = {
  store: PropTypes.object.isRequired
};

export default Hero;
