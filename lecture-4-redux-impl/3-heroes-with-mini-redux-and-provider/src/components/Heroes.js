import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Hero from './Hero';

class Heroes extends Component {
  state = {};

  componentWillMount() {
    const { subscribe, dispatch, getState } = this.context.store;

    fetch('//swapi.co/api/people')
      .then(res => res.json())
      .then(data => data.results)
      .then(heroes => dispatch({ type: 'SAVE_HEROES', heroes }));

    subscribe(() => {
      this.setState(state => ({ ...state, heroes: getState().heroes}))
    });
  }

  render() {
    if (!this.state.heroes) { return null; }

    return (
      <nav>
        {this.state.heroes.map((hero, index) => <Hero key={hero.name} index={index} hero={hero} />)}
      </nav>
    );
  }
}

Heroes.contextTypes = {
  store: PropTypes.object.isRequired
};

export default Heroes;
