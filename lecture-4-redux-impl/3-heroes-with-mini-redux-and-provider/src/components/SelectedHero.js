import React, { Component } from 'react';
import PropTypes from 'prop-types';

class SelectedHero extends Component {
  state = {};
  
  static contextTypes = {
    store: PropTypes.object.isRequired
  };

  componentWillMount() {
    const { subscribe, getState } = this.context.store;

    subscribe(() => {
      const { heroes, selectedHeroIndex } = getState();
      this.setState(state => ({ ...state, hero: heroes[selectedHeroIndex] }))
    });
  }

  render() {
    if (!this.state.hero) { return null; }

    return (
      <div>{this.state.hero.name}</div>
    );
  }
}

export default SelectedHero;
