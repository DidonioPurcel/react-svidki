## additional readings:
* (Redux documentation) http://redux.js.org/
* (Redux course by Dan Abramov) https://egghead.io/series/getting-started-with-redux
* (CQRS and redux) https://medium.com/@swazza85/understanding-redux-as-a-cqrs-system-177526aa4671
* (SOME PROBLEMS WITH REACT/REDUX) https://staltz.com/some-problems-with-react-redux.html
* (Repatch - the simplified redux) https://community.risingstack.com/repatch-the-simplified-redux/ 