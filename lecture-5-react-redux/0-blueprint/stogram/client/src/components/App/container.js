import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import App from './component';

class AppContainer extends Component {
  static contextTypes = {
    store: PropTypes.object.isRequired
  };

  constructor(props, context) {
    super();

    this.state = {
      user: context.store.getState().user
    }
  }

  componentWillMount() {
    this.context.store.subscribe(() => {
      this.setState({ user: this.context.store.getState().user });
    });
  }

  render() {
    return <App {...this.props} user={this.state.user}  />
  }
}

export default withRouter(AppContainer);
