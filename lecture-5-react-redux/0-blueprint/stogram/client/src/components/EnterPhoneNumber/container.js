import React, { Component } from 'react';
import PropTypes from 'prop-types';
import EnterPhoneNumberComponent from './component';

class EnterPhoneNumberContainer extends Component {
  static contextTypes = {
    store: PropTypes.object.isRequired
  };

  handleAuthCodeData = (auth) => {
    console.log(auth);
    this.context.store.dispatch({
      type: 'SAVE_AUTH_DATA',
      auth
    });
  };

  render() {
    return <EnterPhoneNumberComponent {...this.props} handleAuthCodeData={this.handleAuthCodeData} />
  }
}

export default EnterPhoneNumberContainer;
