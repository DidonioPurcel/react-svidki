import React, { Component } from 'react';
import PropTypes from 'prop-types';
import LoginWithCodeComponent from './component';

class LoginWithCodeContainer extends Component {
  static contextTypes = {
    store: PropTypes.object.isRequired
  };

  constructor(props, context) {
    super();

    this.state = {
      auth: context.store.getState().auth
    };
  }

  componentWillMount() {
    this.context.store.subscribe(() => {
      this.setState({ auth: this.context.store.getState().auth });
    });
  }

  handleUserSignIn = (user) => {
    this.context.store.dispatch({
      type: 'SAVE_USER',
      user
    })
  };

  render() {
    if (!this.state.auth) { return null; }

    return (
      <LoginWithCodeComponent
        {...this.props}
        auth={this.state.auth}
        handleUserSignIn={this.handleUserSignIn}
      />
    );
  }
}

export default LoginWithCodeContainer;
