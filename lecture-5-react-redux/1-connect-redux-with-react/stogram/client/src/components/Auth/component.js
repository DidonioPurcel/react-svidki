import React from 'react';
import { Route, Switch } from 'react-router-dom';
import EnterPhoneNumber from '../EnterPhoneNumber';
import LoginWithCode from '../LoginWithCode';

const Auth = ({ match }) => (
  <Switch>
    <Route exact path={match.path} component={EnterPhoneNumber} />
    <Route path={`${match.path}/login-code`} component={LoginWithCode} />
  </Switch>
);

export default Auth;
