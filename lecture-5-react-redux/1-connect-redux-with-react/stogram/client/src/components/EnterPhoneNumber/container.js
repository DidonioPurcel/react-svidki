import { connect } from 'react-redux';
import EnterPhoneNumberComponent from './component';

const mapStateToProps = dispatch => ({
  handleAuthCodeData: auth => {
    dispatch({
      type: 'SAVE_AUTH_DATA',
      auth
    });
  }
});

export default connect(null, mapStateToProps)(EnterPhoneNumberComponent);
