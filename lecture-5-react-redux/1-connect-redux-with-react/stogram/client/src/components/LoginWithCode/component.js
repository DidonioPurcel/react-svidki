import React, { Component } from 'react';

class LoginWithCode extends Component {
  state = {
    code: ''
  };

  handleSubmit = (ev) => {
    ev.preventDefault();

    fetch(
      '/api/auth/sign-in',
      {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          phone: this.props.auth.phone,
          phone_code_hash: this.props.auth.phoneCodeHash,
          code: this.state.code
        })
      }
    )
      .then(res => res.json())
      .then(({ user }) => this.props.handleUserSignIn(user))
      .then(() => this.props.history.push('/'));
  };

  handleCodeChange = (ev) => {
    this.setState({ code: ev.target.value });
  };

  render() {
    return (
      <div className="login-wrapper">
        <h4 className="login-header">{this.props.auth.phone}</h4>
        <p className="login-description">
          We've sent the code to you via sms or mobile app.
          Please enter the code below.
        </p>

        <form className="clear" onSubmit={this.handleSubmit}>
          <label className="login-label">
            Code
            <input
              value={this.state.code}
              onChange={this.handleCodeChange}
              type="text"
              name="code"
              placeholder="Enter your code"
            />
          </label>

          <button className="login-submit" type="submit">
            <h4>Next</h4>
          </button>
        </form>
      </div>
    );
  }
}

export default LoginWithCode;
