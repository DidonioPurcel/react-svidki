import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import './index.css';
import App from './components/App';

const reducer = (state = {}, action) => {
  switch (action.type) {
    case 'SAVE_AUTH_DATA':
      return {
        ...state,
        auth: {
          ...state.auth,
          ...action.auth
        }
      };
    case 'SAVE_USER':
      return {
        ...state,
        user: {
          ...state.user,
          ...action.user
        }
      };
    default:
      return state;
  }
};

const initialState = {
  user: null,
  auth: null
};

const store = createStore(reducer, initialState);

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <App />
    </Router>
  </Provider>,
  document.getElementById('root')
);
