import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import './index.css';
import App from './components/App';

import { auth, user } from './components/Auth/reducers';

const initialState = {
  user: null,
  auth: null
};

const store = createStore(
  combineReducers({
    auth,
    user
  }),
  initialState
);

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <App />
    </Router>
  </Provider>,
  document.getElementById('root')
);
