const { MTProto } = require('telegram-mtproto');
const config = require('../config');

module.exports = MTProto({
  api: {
    invokeWithLayer: 0xda9b0d0d,
    layer          : 57,
    initConnection : 0x69796de9,
    api_id         : config.app.id,
    app_version    : config.app.version,
    lang_code      : config.app.langCode
  },
  server: {
    webogram: true,
    dev: false
  }
});
