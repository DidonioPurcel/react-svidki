import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import Auth from '../Auth';

const App = ({ user = {} }) => (
  <Switch>
    <Route path='/auth' component={Auth} />
    <Route path='/' render={() => {
      return user ?
        <div>{`${user.first_name} ${user.last_name}`}</div> :
        <Redirect to="/auth" />
    }} />
  </Switch>
);

export default App;