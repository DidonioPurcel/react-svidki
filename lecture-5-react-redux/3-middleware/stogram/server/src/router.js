const router = require('express').Router();
const auth = require('./auth');

router.post('/auth/send-code', (req, res) => auth.sendCode(req).then(data => res.send(data)));
router.post('/auth/sign-in', (req, res) => auth.signIn(req).then(data => res.send(data)));

module.exports = router;
