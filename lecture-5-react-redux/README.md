### Container vs presentational components (aka smart and dumb components)																									

### Connecting React with Redux:
  * Inconvenient way
  * SimpleWay

### How to use inputs with redux and react. Synthetic events	
																								
### Combining reducers. Reducers' composition

### Interception of actions using middlewares																									

### Selectors. Reselect library																									

### Hometask
Please, take a look at homework/README.md

### Additional readings:
* (Connecting React with Redux) http://redux.js.org/docs/basics/UsageWithReact.html
* (Understanding middleware) http://redux.js.org/docs/advanced/Middleware.html
* (Presentational and Container Components) https://medium.com/@dan_abramov/smart-and-dumb-components-7ca2f9a7c7d0
* (Reducers' composition) http://redux.js.org/docs/recipes/reducers/UsingCombineReducers.html