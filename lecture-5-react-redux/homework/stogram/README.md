### Homework

* Component 'Dialogs' receives all dialogs (fetch in componentWillMount).
Write a container for this component so it passes two things to component:
function, that keeps a dispatcher, which commands to save dialogs data to redux store;
passes dialogs data from redux. Also, you need to write reducer function.
Don't forget to import file with your reducer to store.js

* Component 'Dialogs' receives all dialogs, but gets it normalized. 
So instead of nested structure you receive dialogs, users and messages separately.
You have to compose this data in your dialogs container so it looks like
one array of dialogs. Example: [{id: 1, users: [], message: ''}]
