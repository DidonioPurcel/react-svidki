import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import Auth from '../Auth';
import Home from '../Home';

const App = ({ user }) => (
  <Switch>
    <Route path='/auth' render={({ match }) => <Auth match={match} user={user} />} />
    <Route path='/' render={() => {
      return user ?
        <Home /> :
        <Redirect to="/auth" />
    }} />
  </Switch>
);

export default App;