import React, { Component } from 'react';
import Dialog from '../Dialog';

class Dialogs extends Component {

  componentWillMount() {
    fetch('/api/dialogs')
      .then(res => res.json())
      .then(data => console.log(data));
  }

  render() {
    return (
      <div>
        Place to render dialogs. You can use Dialog component here.
      </div>
    );
  }
}

export default Dialogs;
