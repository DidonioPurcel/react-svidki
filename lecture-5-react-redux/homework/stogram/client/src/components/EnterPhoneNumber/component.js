import React, { Component } from 'react';
import './styles.css';

class EnterPhoneNumber extends Component {
  state = {
    phone: ''
  };

  handleSubmit = (ev) => {
    ev.preventDefault();
    const { phone } = this.state;

    fetch(
      '/api/auth/send-code',
      {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ phone })
      }
    )
      .then(res => res.json())
      .then(data => this.props.handleAuthCodeData({
        phone,
        phoneCodeHash: data.phone_code_hash
      }))
      .then(() => {
        this.props.history.push(`${this.props.match.path}/login-code`);
      });
  };

  handlePhoneChange = ev => {
    this.setState({ phone: ev.target.value });
  };

  render() {

    return (
      <div className="login-wrapper">
        <h4 className="login-header">Sign in</h4>
        <p className="login-description">
          Please enter your full phone number linked with telegram account
        </p>

        <form className="clear" onSubmit={this.handleSubmit}>
          <label className="login-label">
            Phone number
            <input
              value={this.state.phone}
              onChange={this.handlePhoneChange}
              type="phone"
              name="phone"
              placeholder="380930000000"
            />
          </label>

          <button className="login-submit" type="submit">
            <h4>Next</h4>
          </button>
        </form>
      </div>
    );
  }
}

export default EnterPhoneNumber;
