import { createStore, combineReducers } from 'redux';
import { auth, user } from './components/Auth/reducers';

const userData = localStorage.getItem('user');

const defaultInitialState = {
  user: userData ? JSON.parse(userData) : null,
  auth: null,
  dialogs: null
};

export default (initialState) => {
  const store = createStore(
    combineReducers({
      user,
      auth
    }),
    {
      ...defaultInitialState,
      ...initialState
    }
  );

  store.subscribe(() => {
    localStorage.setItem('user', JSON.stringify(store.getState().user));
  });

  return store;
};
