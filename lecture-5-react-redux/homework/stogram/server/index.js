const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const config = require('./config');
const router = require('./src/router');

const app = express();
const port = process.env.PORT || config.port;

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/api', router);
app.listen(port, () => console.log(`Server is listening on port ${port}`));

// const login = require('./login');
// const { getChat, chatHistory, searchUsers } = require('./chat-history');
// const updateProfile = require('./update-profile');

// const run = async () => {
//   const user = await login();
//   // const res = await searchUsers();
//   // await updateProfile(first_name);
//   // const chat = await getChat()
//   // await chatHistory(chat)
// };
//
// run();
