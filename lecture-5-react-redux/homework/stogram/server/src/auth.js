const telegram = require('./init');
const config = require('../config');

module.exports = {
  sendCode: (req) => {
    return telegram('auth.sendCode', {
      phone_number  : req.body.phone,
      api_id        : config.app.id,
      api_hash      : config.app.hash
    })
      .then(response => {
        console.log(response);
        return response;
      })
      .catch(err => {
        console.log(err);
        throw err;
      });
  },
  signIn: (req) => {
    const signInData = {
      phone_number: req.body.phone,
      phone_code_hash: req.body.phone_code_hash,
      phone_code  : req.body.code
    };

    return telegram('auth.checkPhone', {
      phone_number: req.body.phone
    })
      .then(checkResponse => {
        console.log('Check response', req.body.phone);
        console.log(checkResponse);

        return telegram('auth.signIn', signInData)
          .then(response => {
            console.log(response);
            global.user = response;
            return response;
          })
          .catch(err => {
            console.log(err);
              throw err;
          });
      });
  }
};
