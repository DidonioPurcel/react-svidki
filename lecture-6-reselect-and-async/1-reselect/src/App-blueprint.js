import React, {Component} from 'react';
import './App.css';
import {Provider, connect} from 'react-redux';
import {createStore} from 'redux';

//state

const initialState = {
    names: ['Alex', 'Vasia', 'Petya', 'Serioja', 'Dima', 'Anna', 'Masha', 'Sveta', 'Sasha'],
};

const store = createStore(state => state, initialState);

//Names list

const NamesList = ({names}) => (
    <div className="App">
        {
            names.map(name => <div>{name}</div>)
        }
    </div>
);

const ConnectedNamesList = connect(
    (state) => {
        debugger;
        return {
            names: state.names,
        }
    }
)(NamesList);


//App

class App extends Component {
    render () {
        return (
            <Provider store={store}>
                <ConnectedNamesList/>
            </Provider>
        );
    }
}

export default App;
