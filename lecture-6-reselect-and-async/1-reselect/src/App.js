import React, {Component} from 'react';
import './App.css';
import {Provider, connect} from 'react-redux';
import {createStore, combineReducers} from 'redux';
import {createSelector} from 'reselect';

//state

const initialState = {
    names: ['Alex', 'Vasia', 'Petya', 'Serioja', 'Dima', 'Anna', 'Masha', 'Sveta', 'Sasha'],
    filterBy: '',
    sortByAsc: true,
};

const reducer = combineReducers({
    names: (state = initialState) => state,
    filterBy: (state = '', action) => action.type === 'filter-input' ? action.payload : state,
    sortByAsc: (state = true, action) => action.type === 'sort-change' ? (!state) : state,
});

const store = createStore(reducer, initialState);

//selectors

const getNames = state => state.names;
const getSortByAsc = state => state.sortByAsc;
const getFilterBy = state => state.filterBy;


const makeComparator = (orderAsc) => {
    return (name1, name2) => {
        return orderAsc
            ? name1.localeCompare(name2)
            : name2.localeCompare(name1);
    };
};

const getSortedNames = (state) => getNames(state)
    .slice()
    .sort(makeComparator(getSortByAsc(state)));

const getFilteredNames = (state) => {
    const filterBy = getFilterBy(state);
    return getSortedNames(state)
        .filter(name => name.includes(filterBy));
};

// const getSortedNames = createSelector(
//     getNames,
//     getSortByAsc,
//     (names, sortByAsc) => names.slice().sort(makeComparator(sortByAsc))
// );
//
// const getFilteredNames = createSelector(
//     getSortedNames,
//     getFilterBy,
//     (sortedNames, filterBy) => {
//         return sortedNames
//             .filter(name => name.includes(filterBy));
//     }
// );

//Names list

const NamesList = ({names}) => (
    <div className="App">
        {
            names.map(name => <div key={name}>{name}</div>)
        }
    </div>
);

const ConnectedNamesList = connect(
    (state) => {
        return {
            names: getFilteredNames(state),
        };
    }
)(NamesList);

//Filter

const FilterInput = ({onInput}) => (
    <input type="text" onInput={onInput}/>
);

const ConnectedFilterInput = connect(null, dispatch => ({
    onInput (event) {
        dispatch({
            type: 'filter-input',
            payload: event.target.value,
        });
    }
}))(FilterInput);

//Sort

const SortButton = ({onClick}) => (
    <button onClick={onClick}>Change Sorting order</button>
);

const ConnectedSortButton = connect(null, dispatch => ({
    onClick () {
        dispatch({
            type: 'sort-change',
        });
    }
}))(SortButton);


//App

class App extends Component {
    render () {
        return (
            <Provider store={store}>
                <div>
                    <ConnectedSortButton/>
                    <ConnectedFilterInput/>
                    <ConnectedNamesList/>
                </div>
            </Provider>
        );
    }
}

export default App;
