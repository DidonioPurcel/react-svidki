import React, {Component} from 'react';
import './App.css';
import {Provider, connect} from 'react-redux';
import {store, getFilledDialogs} from './state';
import {loadDialogs} from './state';
import {getDialogs as smthGetDialogs} from './state/async/something';
import {getDialogs as thunkGetDialogs} from './state/async/thunk';
import {isDataLoading} from './state/selectors';

const Member = ({name}) => (
    <span className="dialog-member">{name}</span>
);

const Members = ({members}) => (
    <div>
        <span>Members:</span>
        {
            members.map(m => <Member key={m.name} name={m.name}/>)
        }
    </div>
);

const Dialog = ({name, id, members}) => (
    <div className="dialog">
        <div>
            Name: {name}
        </div>
        <Members members={members}/>
    </div>
);

const Dialogs = ({dialogs}) => (
    <div>
        {
            dialogs.map(d =>
                <Dialog key={d.id} name={d.name} id={d.id} members={d.members}/>
            )
        }
    </div>
);

class DialogsScreen extends Component {
    componentDidMount () {
        this.props.load();
    }

    render () {
        const {dialogs, loading} = this.props;

        return (
            <div>
                {
                    loading
                        ? (<div>Loading data...</div>)
                        : (<Dialogs dialogs={dialogs}/>)
                }
            </div>
        );
    }
}

const ConnectedDialogsScreen = connect(
    state => ({
        dialogs: getFilledDialogs(state),
        loading: isDataLoading(state)
    }),
    dispatch => ({
        load: () => {
            //smthGetDialogs(dispatch);
            //dispatch(thunkGetDialogs());
            dispatch(loadDialogs());
        }
    })
)(DialogsScreen);

class App extends Component {
    render () {
        return (
            <Provider store={store}>
                <ConnectedDialogsScreen/>
            </Provider>
        );
    }
}

export default App;
