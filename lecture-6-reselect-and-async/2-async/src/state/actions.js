export const LOAD_DIALOGS_ACTION = 'LOAD_DIALOG_ACTION';
export const LOADED_DIALOGS_ACTION = 'LOADED_DIALOGS_ACTION';
export const LOAD_CONTACTS_ACTION = 'LOAD_CONTACTS_ACTION';
export const LOADED_CONTACTS_ACTION = 'LOADED_CONTACTS_ACTION';
export const DELETE_DIALOG_ACTION = 'DELETE_DIALOG_ACTION';
export const DELETED_DIALOG_ACTION = 'DELETED_DIALOG_ACTION';
export const CREATE_DIALOG_ACTION = 'CREATE_DIALOG_ACTION';
export const CREATED_DIALOG_ACTION = 'CREATED_DIALOG_ACTION';

export const LOADING_STARTED = 'LOADING_STARTED';
export const LOADING_ENDED = 'LOADING_ENDED';

export const loadingStarted = () => ({
    type: LOADING_STARTED,
});

export const loadingEnded = () => ({
    type: LOADING_ENDED,
});

export const loadContacts = () => ({
    type: LOAD_CONTACTS_ACTION
});

export const loadedContacts = (contacts) => ({
    type: LOADED_CONTACTS_ACTION,
    payload: contacts,
});

export const loadDialogs = () => ({
    type: LOAD_DIALOGS_ACTION,
});

export const loadedDialogs = (dialogs) => ({
    type: LOADED_DIALOGS_ACTION,
    payload: dialogs,
});

export const deleteDialog = (id) => ({
    type: DELETE_DIALOG_ACTION,
    payload: id,
});

export const deletedDialog = (id) => ({
    type: DELETED_DIALOG_ACTION,
    payload: id,
});

export const createDialog = (name) => ({
    type: CREATE_DIALOG_ACTION,
    payload: name,
});

export const createdDialog = (dialog) => ({
    type: CREATED_DIALOG_ACTION,
    payload: dialog,
});