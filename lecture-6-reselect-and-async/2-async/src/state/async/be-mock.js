const dialogs = [
    {
        id: 'first',
        members: [1, 2],
        name: 'Backbase',
    },
    {
        id: 'second',
        members: [2, 3],
        name: 'Frontend-divan'
    },
    {
        id: 'third',
        members: [1, 2, 3],
        name: 'Frontend-Tromb',
    },
];

const contacts = [
    {
        id: 1,
        name: 'Yuriy Kabay',
    },
    {
        id: 2,
        name: 'Andrey Rovenko',
    },
    {
        id: 3,
        name: 'Alexey Lvov',
    },
];

export const fetchDialogs = () => {
    return new Promise(resolve => {
        setTimeout(() => resolve(dialogs), 1000);
    });
};

export const createDialog = (name) => {
    return new Promise(resolve => {
        setTimeout(() => resolve({name, id: name.toLowerCase(), members: []}), 1000);
    });
};

export const deleteDialog = (id) => {
    return new Promise(resolve => {
        setTimeout(() => resolve({id}), 1000);
    });
};


export const fetchContacts = () => {
    return new Promise(resolve => {
        setTimeout(() => resolve(contacts), 2000);
    });
};