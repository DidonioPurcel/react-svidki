import {combineEpics} from 'redux-observable';
import {
    LOAD_DIALOGS_ACTION, LOADED_DIALOGS_ACTION, loadedContacts, loadedDialogs, loadingEnded,
    loadingStarted
} from '../actions';
import {Observable} from 'rxjs';
import 'rxjs/add/observable/fromPromise';
import {fetchContacts, fetchDialogs} from './be-mock';

const dialogsEpic = ($actions) => {
    return $actions.ofType(LOAD_DIALOGS_ACTION)
        .mergeMap(() => {
            return Observable
                .fromPromise(fetchDialogs())
                .map(loadedDialogs);
        });
};

const contactsEpic = ($actions) => {
    return $actions.ofType(LOAD_DIALOGS_ACTION)
        .mergeMap(() => {
            return Observable
                .fromPromise(fetchContacts())
                .map(loadedContacts);
        });
};

const loadingStartedEpic = ($actions) => {
    return $actions.ofType(LOAD_DIALOGS_ACTION)
        .map(loadingStarted);
};

const loadingEndedEpic = ($actions) => {
    return $actions.ofType(LOADED_DIALOGS_ACTION)
        .map(loadingEnded);
};

export const epic = combineEpics(
    dialogsEpic,
    contactsEpic,
    loadingStartedEpic,
    loadingEndedEpic
);