import {fetchContacts, fetchDialogs} from './be-mock';
import {
    LOAD_DIALOGS_ACTION, LOADED_DIALOGS_ACTION, loadedContacts, loadedDialogs, loadingEnded,
    loadingStarted
} from '../actions';
import {takeEvery, put} from 'redux-saga/effects';

function* dialogsSaga () {
    const dialogs = yield fetchDialogs();
    yield put(loadedDialogs(dialogs));
}

function* contactsSaga () {
    const contacts = yield fetchContacts();
    yield put(loadedContacts(contacts));
}

function* loadingSaga () {
    yield put(loadingStarted());
}

function* loadingEndedSaga () {
    yield put(loadingEnded());
}

export function* saga () {
    yield takeEvery(LOAD_DIALOGS_ACTION, dialogsSaga);
    yield takeEvery(LOAD_DIALOGS_ACTION, contactsSaga);
    yield takeEvery(LOAD_DIALOGS_ACTION, loadingSaga);
    yield takeEvery(LOADED_DIALOGS_ACTION, loadingEndedSaga);
}