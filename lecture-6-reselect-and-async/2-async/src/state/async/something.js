import {fetchContacts, fetchDialogs} from './be-mock';
import {loadedDialogs, loadedContacts} from '../index';
import {loadingEnded, loadingStarted} from '../actions';

/**
 * TODO: Talk about optimistic update
 */
export const getDialogs = dispatch => {
    dispatch(loadingStarted());

    fetchDialogs()
        .then(dialogs => dispatch(loadedDialogs(dialogs)))
        .then(() => dispatch(loadingEnded()));

    fetchContacts()
        .then(contacts => dispatch(loadedContacts(contacts)));
};