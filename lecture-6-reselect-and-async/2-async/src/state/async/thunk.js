import {fetchContacts, fetchDialogs} from './be-mock';
import {loadedDialogs, loadedContacts} from '../index';
import {loadingEnded, loadingStarted} from '../actions';

export const getDialogs = () => {
    return dispatch => {
        dispatch(loadingStarted());

        fetchDialogs()
            .then(dialogs => dispatch(loadedDialogs(dialogs)))
            .then(() => dispatch(loadingEnded()));

        fetchContacts()
            .then(contacts => dispatch(loadedContacts(contacts)));
    }
};