import {createStore, applyMiddleware} from 'redux';
import {reducer} from './reducers';

//redux observable
import {createEpicMiddleware} from 'redux-observable';
import {epic} from './async/observable';

//saga
import createSagaMiddleware from 'redux-saga'
import {saga} from './async/saga';

//thunk
import thunk from 'redux-thunk';


//redux-observable
const epicMiddleware = createEpicMiddleware(epic);

//redux saga
const sagaMiddleware = createSagaMiddleware();


export * from './selectors';
export * from './actions';
export const store = createStore(
    reducer,
    applyMiddleware(
        thunk,
        //epicMiddleware,
        sagaMiddleware
    ),
);

sagaMiddleware.run(saga);