import {
    CREATED_DIALOG_ACTION, DELETE_DIALOG_ACTION,
    LOADED_CONTACTS_ACTION,
    LOADED_DIALOGS_ACTION, LOADING_ENDED, LOADING_STARTED
} from './actions';
import {combineReducers} from 'redux';

const dialogs = (state = [], action) => {
    switch (action.type) {
        case LOADED_DIALOGS_ACTION:
            return action.payload;
        case CREATED_DIALOG_ACTION:
            return [
                ...state,
                action.payload,
            ];
        case DELETE_DIALOG_ACTION:
            return state.filter(item => {
                return item.id !== action.payload;
            });
        default:
            return state;
    }
};

const contacts = (state = [], action) => {
    switch (action.type) {
        case LOADED_CONTACTS_ACTION:
            return [
                ...state,
                ...action.payload,
            ];
        default:
            return state;
    }
};

const loading = (state = false, action) => {
    switch (action.type) {
        case LOADING_STARTED:
            return true;
        case LOADING_ENDED:
            return false;
        default:
            return state;
    }
};

export const reducer = combineReducers({
    dialogs,
    contacts,
    loading,
});