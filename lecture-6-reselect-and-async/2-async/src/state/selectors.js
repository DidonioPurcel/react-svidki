import {createSelector} from 'reselect';

export const dialogsSelector = state => state.dialogs;
export const contactsSelector = state => state.contacts;
export const isDataLoading = state => state.loading;

export const getFilledDialogs = createSelector(
    dialogsSelector,
    contactsSelector,
    (dialogs, contacts) => {
        return dialogs.map(dialog => {
            const {members} = dialog;
            return {
                ...dialog,
                members: contacts.filter(({id}) => members.includes(id))
            };
        });
    }
);
