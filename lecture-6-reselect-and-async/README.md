### What is reselect
  * library to create memorized selectors (caching by arguments)
    
### Redux is synchronous, so all actions appear in reducer synchronously.
  * as the result, code is clean and easily reasonable

### It is up to you how to manage side-effects or async stuff;

### The simplest way - redux-thunk 
(https://github.com/gaearon/redux-thunk/blob/master/src/index.js)

### Reactive way (epics): redux-observable

### Generators way (sagas): redux-saga

### Homework

Rewrite all your asynchronous calls:
* Using redux-thunk
* Using redux-observable or redux-saga
* Have fun:)

### Additional readings:
* Reselect library (https://github.com/reactjs/reselect#motivation-for-memoized-selectors)
* Async actions (http://redux.js.org/docs/advanced/AsyncActions.html)
* Redux-thunk (https://github.com/gaearon/redux-thunk)
* Generators (http://exploringjs.com/es6/ch_generators.html)
* Redux-saga (https://github.com/redux-saga/redux-saga)
* RX.js (http://reactivex.io/)
* Redux-observable (https://github.com/redux-observable/redux-observable)
* Redux 4 ways (https://medium.com/react-native-training/redux-4-ways-95a130da0cdc)
