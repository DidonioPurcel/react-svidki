import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import AppComponent from './component';

const mapStateToProps = state => ({
  user: state.user
});

export default withRouter(connect(mapStateToProps)(AppComponent));
