import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import EnterPhoneNumber from '../EnterPhoneNumber';
import LoginWithCode from '../LoginWithCode';

const Auth = ({ match, user }) => (
  <div>
    {user ?
      <Redirect to='/' /> :
      <Switch>
        <Route exact path={match.path} component={EnterPhoneNumber} />
        <Route path={`${match.path}/login-code`} component={LoginWithCode} />
      </Switch>
    }
  </div>
);

export default Auth;
