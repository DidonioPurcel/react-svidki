export const auth = (state = {}, action) => {
  switch (action.type) {
    case 'SAVE_AUTH_DATA':
      return {
        ...state,
        ...action.auth
      };
    default:
      return state;
  }
};

export const user = (state = {}, action) => {
  switch (action.type) {
    case 'SAVE_USER':
      return {
        ...state,
        ...action.user
      };
    default:
      return state;
  }
};
