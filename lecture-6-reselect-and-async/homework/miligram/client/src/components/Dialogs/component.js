import React, { Component } from 'react';
import Dialog from '../Dialog';

class Dialogs extends Component {

  componentWillMount() {
    fetch('/api/dialogs')
      .then(res => res.json())
      .then(data => this.props.handleDialogsData({ data }));
  }

  render() {
    console.log(this.props.dialogs);

    return (
      <div>
        {this.props.dialogs.map((dialog, index) => <Dialog key={index} dialog={dialog} />)}
      </div>
    );
  }
}

export default Dialogs;
