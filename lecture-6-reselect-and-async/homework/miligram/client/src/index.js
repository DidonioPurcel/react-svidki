import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import getUser from './user';
import './index.css';
import createStore from './store';
import App from './components/App';

const createApp = (user) => {
  const store = createStore({ user });

  ReactDOM.render(
    <Provider store={store}>
      <Router>
        <App />
      </Router>
    </Provider>,
    document.getElementById('root')
  );
};

getUser().then(createApp);