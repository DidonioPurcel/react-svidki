export default async function getUser() {
  try {
    const response = await fetch('/api/user');
    return await response.json();
  } catch (err) {
    return null;
  }
}
