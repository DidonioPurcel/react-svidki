const router = require('express').Router();
const auth = require('./auth');
const dialogs = require('./dialogs');
const user = require('./user');

router.post('/auth/send-code', (req, res) => auth.sendCode(req).then(data => res.send(data)));
router.post('/auth/sign-in', (req, res) => auth.signIn(req).then(data => res.send(data)));
router.get('/dialogs', (req, res) => dialogs.getDialogs(req).then(data => res.send(data)));
router.get('/user', (req, res) => user.getUser(req).then(data => res.send(data)).catch(err => res.status(err.code).send(err.description)));

module.exports = router;
