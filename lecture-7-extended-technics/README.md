Additional links:
(Normalizing state shape) http://redux.js.org/docs/recipes/reducers/NormalizingStateShape.html
(Normalizr) https://github.com/paularmstrong/normalizr
(Immutable update patterns) http://redux.js.org/docs/recipes/reducers/ImmutableUpdatePatterns.html
(Immutability helper) https://github.com/kolodny/immutability-helper
(ImmutableJS presentation) https://www.youtube.com/watch?v=I7IdS-PbEgI&feature=youtu.be
(Why to use immutable library with redux) http://redux.js.org/docs/recipes/UsingImmutableJS.html#why-use-immutable-library
(Unit testing of react components) https://www.codementor.io/vijayst/unit-testing-react-components-jest-or-enzyme-du1087lh8
(Testing of react components with Jest) https://hackernoon.com/testing-react-components-with-jest-and-enzyme-41d592c174f