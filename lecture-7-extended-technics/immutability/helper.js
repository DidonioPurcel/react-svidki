'use strict';
const update = require('immutability-helper');

const array1 = ['a'];
//array1.push('b');
//array1.concat('b');
const array2 = update(array1, { $push: ['b'] });

console.log('EXAMPLE 1');
console.log(array1);
console.log(array2);
console.log(array1 === array2);

console.log();
console.log('EXAMPLE 2');
const complexArray = [{ a: [{ c: 'f' }] }, { a: [{ c: 'g' }, { c: 'e' }] }];
console.log(JSON.stringify(complexArray));

const complexArray2 = update(complexArray, { 1: { a: { 1: { c: { $set: 'j' } } } } });
console.log(JSON.stringify(complexArray2));


