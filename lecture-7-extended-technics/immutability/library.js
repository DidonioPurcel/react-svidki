'use strict';

function doAnything(state) {
    print(state);
}

function print(state) {
    //some good logic
    state.anything = 'do evil';

    //some good logic again
}


//Map ~~ Object
const Immutable = require('immutable');

const map1 = Immutable.Map({ first: 'a', second: 'b' });

console.log('map1.first =', map1.get('first'));

const map2 = map1.set('first', 'foo');

console.log('map1.first =', map1.get('first'));
console.log('map2.first =', map2.get('first'));

//List ~~ Array
const list = Immutable.List(['p', 'o', 'n', 'c', 'h', 'k']);
console.log(list.get(3));
console.log(list.set(3, 's'));


//fromJs, Records, Sequence etc, OrderedMap (!important)