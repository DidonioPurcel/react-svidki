import React, {Component} from 'react';
import './App.css';
import {connect, Provider} from 'react-redux';
import {store, getUsersTasks, loadData, editComment} from './store/normalized';

const Assignee = ({name, id}) => (
    <div>
        <span><b>Assignee: </b></span>
        <span>{name}</span>
    </div>
);

const Comment = connect(
    null,
    dispatch => ({
        edit: (id, text) => dispatch(editComment(id, text))
    })
)
(({text, author, id, edit}) => (
    <tr>
        <td><b>{author}:</b></td>
        <td>
            <input value={text} onChange={(event) => edit(id, event.target.value)}></input>
        </td>
    </tr>
));

const Comments = ({comments}) => (
    <div>
        <h5>Comments:</h5>
        <table>
            <tbody>
            {
                comments.map(({text, author, id}) => (
                    <Comment text={text}
                             author={author}
                             id={id}
                             key={id}>
                    </Comment>
                ))
            }
            </tbody>
        </table>
    </div>
);

const Task = ({name, comments}) => (
    <div className="task">
        <b>Summary:</b>
        <h2>{name}</h2>
        <Comments comments={comments}/>
    </div>
);

const Tasks = ({tasks}) => (
    <div>
        {
            tasks.map(task => (
                <Task name={task.name}
                      comments={task.comments}
                      key={task.id}>
                </Task>
            ))
        }
    </div>
);

const UserTask = ({user}) => {
    const {name, id, tasks} = user;

    return (
        <div className="user-task">
            <Assignee name={name} id={id}>
            </Assignee>
            <Tasks tasks={tasks}></Tasks>
        </div>
    );
};

const UserTasks = connect(
    state => ({
        users: getUsersTasks(state)
    }),
    dispatch => ({
        loadData: () => dispatch(loadData())
    }))
(class extends Component {
        componentDidMount () {
            this.props.loadData()
        }

        render () {
            return (
                <div>
                    {
                        this.props.users.map(user => (
                            <UserTask user={user}
                                      key={user.id}/>
                        ))
                    }
                </div>
            );
        }
    }
);

class App extends Component {
    render () {
        return (
            <Provider store={store}>
                <div className="App">
                    <h1>Enterprise bakery</h1>
                    <UserTasks>
                    </UserTasks>
                </div>
            </Provider>
        );
    }
}

export default App;
