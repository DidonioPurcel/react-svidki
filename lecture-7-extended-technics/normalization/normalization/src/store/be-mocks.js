export const data = {
    users: [
        {
            name: 'Yuriy Kabai',
            id: 'yuriyk',
            tasks: [
                {
                    id: '1',
                    name: 'Go to grocery, buy products',
                    comments: [
                        {
                            text: 'Please do it ASAP, till tomorrow',
                            id: 'comment1',
                            author: 'Valera',
                        },
                        {
                            text: 'We have to investigate first what grocery is, maybe another type of shop is more appropriate',
                            id: 'comment2',
                            author: 'Ostap',
                        },
                        {
                            text: 'Ok, I will create two-days spike for it',
                            id: 'comment3',
                            author: 'Andrew',
                        },
                    ],
                },
                {
                    id: '2',
                    name: 'Bake a cake',
                    comments: [
                        {
                            text: 'Let`s create a meeting to discuss what flour to use',
                            id: 'commentForBaking1',
                            author: 'Gosha',
                        },
                        {
                            text: 'Can we bake a cake without a flavor? Maybe it will work for MVP release?',
                            id: 'commentForBaking2',
                            author: 'Gosha',
                        },
                    ],
                }
            ]
        },
        {
            name: 'Yevhen Lubianov',
            id: 'yevhenl',
            tasks: [
                {
                    id: '2',
                    name: 'Bake a cake',
                    comments: [
                        {
                            text: 'Let`s create a meeting to discuss what flour to use',
                            id: 'commentForBaking1',
                            author: 'Gosha',
                        },
                        {
                            text: 'Can we bake a cake without a flavor? Maybe it will work for MVP release?',
                            id: 'commentForBaking2',
                            author: 'Gosha',
                        },
                    ],
                }
            ]
        },
    ],
};

export function fetchTasks () {
    return new Promise(resolve => resolve(data));
}

export function changeComment (id, text) {
    return new Promise(resolve => resolve({id, text}));
}