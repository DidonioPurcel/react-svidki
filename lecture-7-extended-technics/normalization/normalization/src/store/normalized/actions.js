import { fetchTasks } from '../be-mocks';
import { usersSchema } from './schema';
import { normalize } from 'normalizr';

export const LOADED_DATA = 'LOADED_DATA';
export const EDITED_COMMENT = 'EDITED_COMMENT';

export const loadedData = (data) => ({
    type: LOADED_DATA,
    payload: data,
});

export const editedComment = (text) => ({
    type: EDITED_COMMENT,
    payload: text,
});

export const editComment = (id, text) => dispatch => {
    // changeComment(id, text)
    //     .then(data => );
    dispatch(editedComment({ id, text }));
};

export const loadData = () => dispatch => {
    fetchTasks().then(data => {
        const normalized = normalize(data.users, usersSchema);

        const result = {
            usersIds: normalized.result,
            users: normalized.entities.users,
            tasks: normalized.entities.tasks,
            comments: normalized.entities.comments,
        };

        console.log(result);
        dispatch(
            loadedData(result)
        )
    })
};