import { createStore, combineReducers, applyMiddleware } from 'redux';
import { denormalize } from 'normalizr';
import thunk from 'redux-thunk';
import { createSelector } from 'reselect';
import { LOADED_DATA, EDITED_COMMENT } from './actions';
import { usersSchema } from './schema';

export * from './actions';

const usersIdsReducer = (state = [], action) => {
    switch (action.type) {
        case LOADED_DATA:
            return action.payload.usersIds;
        default:
            return state;
    }
};

const usersReducer = (state = {}, action) => {
    switch (action.type) {
        case LOADED_DATA:
            return action.payload.users;
        default:
            return state;
    }
};

const commentsReducer = (state = {}, action) => {
    switch (action.type) {
        case LOADED_DATA:
            return action.payload.comments;
        case EDITED_COMMENT:
            return {
                ...state,
                [action.payload.id]: {
                    ...state[action.payload.id],
                    ...action.payload,
                }
            };
        default:
            return state;
    }
};

const tasksReducer = (state = {}, action) => {
    switch (action.type) {
        case LOADED_DATA:
            return action.payload.tasks;
        default:
            return state;
    }
};

const reducers = combineReducers({
    usersIds: usersIdsReducer,
    users: usersReducer,
    tasks: tasksReducer,
    comments: commentsReducer,
});

export const store = createStore(
    reducers,
    applyMiddleware(thunk)
);

const getUsersIds = state => state.usersIds;
const getUsers = state => state.users;
const getTasks = state => state.tasks;
const getComments = state => state.comments;

export const getUsersTasks = createSelector(
    getUsersIds,
    getUsers,
    getTasks,
    getComments,
    (userIds, users, tasks, comments) => {
        return userIds.map(id => ({
            ...users[id],
            tasks: users[id].tasks.map(taskId => ({
                ...tasks[taskId],
                comments: tasks[taskId].comments.map(commentId => comments[commentId])
            }))
        }));
        //return denormalize(userIds, usersSchema, {users, tasks, comments});
    }
);