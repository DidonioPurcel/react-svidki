import {schema} from 'normalizr';

export const commentSchema = new schema.Entity('comments');
export const commentsSchema = new schema.Array(commentSchema);
export const taskSchema = new schema.Entity('tasks', {comments: commentsSchema});
export const tasksSchema = new schema.Array(taskSchema);
export const userSchema = new schema.Entity('users', {tasks: tasksSchema});
export const usersSchema = new schema.Array(userSchema);

