import {fetchTasks, changeComment} from '../be-mocks';

export const LOADED_DATA = 'LOADED_DATA';
export const EDITED_COMMENT = 'EDITED_COMMENT';

export const loadedData = (data) => ({
    type: LOADED_DATA,
    payload: data,
});

export const editedComment = (comment) => ({
    type: EDITED_COMMENT,
    payload: comment,
});

export const editComment = (id, text) => dispatch => {
    // changeComment(id, text)
    //     .then(data => );
    dispatch(editedComment({id, text}));
};

export const loadData = () => dispatch => {
    fetchTasks()
        .then(data => dispatch(loadedData(data.users)))
};