import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {usersReducer} from './reducers';

export * from './actions';

const reducers = combineReducers({
    users: usersReducer
});

export const store = createStore(
    reducers,
    applyMiddleware(thunk)
);

export const getUsersTasks = (state) => {
    return state.users;
};