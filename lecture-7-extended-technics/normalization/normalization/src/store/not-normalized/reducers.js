import {LOADED_DATA, EDITED_COMMENT} from './actions';

const commentReducer = (state, action) => {
    switch (action.type) {
        case EDITED_COMMENT:
            if (action.payload.id === state.id) {
                return {
                    ...state,
                    ...action.payload,
                };
            }
            return state;
        default:
            return state;
    }
};

const commentsReducer = (state, action) => {
    switch (action.type) {
        case EDITED_COMMENT:
            return state.map(comment => commentReducer(comment, action));
        default:
            return state;
    }
};

const taskReducer = (state, action) => {
    switch (action.type) {
        case EDITED_COMMENT:
            return {
                ...state,
                comments: commentsReducer(state.comments, action),
            };
        default:
            return state;
    }
};

const tasksReducer = (state, action) => {
    switch (action.type) {
        case EDITED_COMMENT:
            return state
                .map(task => taskReducer(task, action));
        default:
            return state;
    }
};

const userReducer = (state, action) => {
    switch (action.type) {
        case EDITED_COMMENT:
            return {
                ...state,
                tasks: tasksReducer(state.tasks, action)
            };
        default:
            return state;
    }
};

export const usersReducer = (state = [], action) => {
    switch (action.type) {
        case LOADED_DATA:
            return action.payload;
        case EDITED_COMMENT:
            return state.map(user => userReducer(user, action));
        default:
            return state;
    }
};