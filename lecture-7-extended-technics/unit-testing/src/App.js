import React, {Component} from 'react';
import './App.css';

export const MyComponent = ({foo}) => (
    <div className=".foo">{foo}</div>
);


class App extends Component {
    render () {
        return (
            <div>
                <MyComponent
                    foo="bar"
                    onClick={this.props.doSmth}/>
            </div>
        );
    }
}

export default App;
