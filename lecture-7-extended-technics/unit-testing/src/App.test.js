import React from 'react';
import ReactDOM from 'react-dom';
import App, {MyComponent} from './App';
import {shallow} from 'enzyme';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({adapter: new Adapter()});

describe('App', () => {
    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<App/>, div);
    });

    describe('content', () => {
        let shallowWrapper;
        let clickHandler;
        beforeEach(() => {
            clickHandler = jest.fn();
            shallowWrapper = shallow(<App doSmth={clickHandler}></App>);
        });

        it('should render MyComponent', () => {
            expect(shallowWrapper.find(MyComponent).length).toBe(1);
        });

        it('should react on click on MyComponent', () => {
            shallowWrapper.find(MyComponent).simulate('click');
            expect(clickHandler).toHaveBeenCalled();
        });

        it('shallow equal should not render items deeply', () => {
            expect(shallowWrapper.find('.foo').length).toBe(0);
        });
    });
});

