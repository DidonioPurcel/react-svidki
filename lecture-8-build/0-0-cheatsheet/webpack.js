const webpack = require('webpack');
const path = require('path');
const precss = require('precss');
const autoprefixer = require('autoprefixer');
const { Config } = require('webpack-config');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const isDev = process.env.NODE_ENV !== 'production';
const isProd = process.env.NODE_ENV === 'production';

const config = new Config();

const entry = [
  'react-hot-loader/patch',
  './src/index.js'
];

const output = {
  filename: 'main.[hash].js',
  path: path.resolve(__dirname, 'dist'),
  chunkFilename: '[name].bundle.js',
  publicPath: '/'
};

const jsLoader = {
  test: /\.(js|jsx)$/,
  use: ['babel-loader', 'eslint-loader']
};

const cssLoader = {
  test: /\.css$/,
  use: [
    'style-loader',
    {
      loader: 'css-loader',
      options: {
        importLoaders: 1,
        modules: true,
        camelCase: true,
        localIdentName: '[local]__[hash:base64:5]'
      }
    }
  ]
};

const html = new HtmlWebpackPlugin({
  template: path.resolve(__dirname, 'public/index.html'),
  inject: 'body'
});

const uglify = new webpack.optimize.UglifyJsPlugin();

const environment = new webpack.EnvironmentPlugin({ NODE_ENV: process.env.NODE_ENV });

const options = {
  devtool: 'source-map',
  context: path.resolve(__dirname),
  bail: isProd,
  resolve: {
    extensions: ['.css', '.js', '.jsx', '.json']
  }
};

// Splitting

const devServer = {
  output: {
    pathinfo: true
  },
  devServer: {
    contentBase: path.resolve(__dirname, 'public'),
    publicPath: '/',
    hot: true,
    inline: true,
    historyApiFallback: true,
    stats: {
      colors: true,
      chunks: false,
      'errors-only': true
    },
    headers: { 'Access-Control-Allow-Origin': '*' },
    open: true,
    port: 8090,
    proxy: {
      '/api': {
        target: 'http://localhost:8080',
        secure: false
      }
    },
  },
  plugins: [
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin()
  ]
};

const prod = {
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin(),
    new CleanWebpackPlugin(['dist'], {
      root: __dirname,
      verbose: false,
      dry: false
    }),
    new CopyWebpackPlugin([
      { from: 'public' }
    ])
  ]
};

// HMR

const entry = [
  'react-hot-loader/patch',
  './src/index.js'
];

// CSS modules