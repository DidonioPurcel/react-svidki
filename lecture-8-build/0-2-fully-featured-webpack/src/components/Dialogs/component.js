import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Dialog from '../Dialog';

class Dialogs extends Component {
  static propTypes = {
    dialogs: PropTypes.array.isRequired,
    handleDialogsData: PropTypes.func.isRequired,
  };

  componentWillMount() {
    fetch('/api/dialogs')
      .then(res => res.json())
      .then(data => this.props.handleDialogsData({ data }));
  }

  render() {
    return (
      <div>
        {this.props.dialogs.map((dialog, index) => <Dialog key={index} dialog={dialog} />)}
      </div>
    );
  }
}

export default Dialogs;
