import { connect } from 'react-redux';
import Component from './component';

const mapStateToProps = ({ dialogs }) => ({
  dialogs: dialogs ? dialogs.data.dialogs : []
});

const mapDispatchToProps = dispatch => ({
  handleDialogsData: (data) => {
    dispatch({
      type: 'SAVE_DIALOGS',
      data
    });
  }
});


export default connect(mapStateToProps, mapDispatchToProps)(Component);
