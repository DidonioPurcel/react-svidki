export const dialogs = (state = {}, action) => {
  switch (action.type) {
    case 'SAVE_DIALOGS':
      return action.data;
    default:
      return state;
  }
};

export default {
  dialogs
};
