import React from 'react';
import Dialogs from '../Dialogs';
import styles from './styles.css';

const Home = () => (
  <div className={styles.home}>
    <Dialogs />
  </div>
);

export default Home;
