import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from '../EnterPhoneNumber/styles.css';

class LoginWithCode extends Component {
  state = {
    code: ''
  };

  handleSubmit = (ev) => {
    ev.preventDefault();

    fetch(
      '/api/auth/sign-in',
      {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          phone: this.props.auth.phone,
          phone_code_hash: this.props.auth.phoneCodeHash,
          code: this.state.code
        })
      }
    )
      .then(res => res.json())
      .then(({ user }) => this.props.handleUserSignIn(user))
      .then(() => this.props.history.push('/'));
  };

  handleCodeChange = (ev) => {
    this.setState({ code: ev.target.value });
  };

  render() {
    return (
      <div className={styles.loginWrapper}>
        <h4 className={styles.loginHeader}>{this.props.auth.phone}</h4>
        <p className={styles.loginDescription}>
          We have sent the code to you via sms or mobile app.
          Please enter the code below.
        </p>

        <form className='clear' onSubmit={this.handleSubmit}>
          <label htmlFor='code' className={styles.loginLabel}>
            Code
            <input
              id='code'
              value={this.state.code}
              onChange={this.handleCodeChange}
              type='text'
              name='code'
              placeholder='Enter your code'
            />
          </label>

          <button className={styles.loginSubmit} type='submit'>
            <h4>Next</h4>
          </button>
        </form>
      </div>
    );
  }
}

LoginWithCode.propTypes = {
  auth: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  handleUserSignIn: PropTypes.func.isRequired
};

export default LoginWithCode;
