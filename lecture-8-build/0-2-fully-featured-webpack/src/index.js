const hello = () => {
  console.log('Hello, kitty');
};

hello();
// import React from 'react';
// import ReactDOM from 'react-dom';
// import { Provider } from 'react-redux';
// import { AppContainer } from 'react-hot-loader';
// import { BrowserRouter as Router } from 'react-router-dom';
// import getUser from './user';
// import './index.css';
// import createStore from './store';
// import App from './components/App';
//
// let appStore = null;
//
// const render = (store) => {
//   ReactDOM.render(
//     <AppContainer>
//       <Provider store={store}>
//         <Router>
//           <App />
//         </Router>
//       </Provider>
//     </AppContainer>,
//     document.getElementById('root')
//   );
// };
//
// const createApp = (user) => {
//   appStore = createStore({ user });
//   render(appStore);
// };
//
// getUser().then(createApp);
//
// if (module.hot) {
//   module.hot.accept('./components/App', () => { render(appStore); });
// }
