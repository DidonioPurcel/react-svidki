import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import loadAsync from '../loadAsync';
// import Auth from '../Auth';
import Home from '../Home';

const AuthAsync = loadAsync('Auth');

// <Route path='/auth' render={({ match }) => <AuthAsync match={match} user={user} />} />
// <Route path='/auth' render={({ match }) => <Auth match={match} user={user} />} />

const App = ({ user }) => (
  <Switch>
    <Route path='/auth' render={({ match }) => <AuthAsync match={match} user={user} />} />
    <Route
      path='/'
      render={() => (
        user ?
          <Home /> :
          <Redirect to='/auth' />
      )}
    />
  </Switch>
);

App.propTypes = {
  user: PropTypes.object
};

export default App;
