import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import EnterPhoneNumber from '../EnterPhoneNumber';
import LoginWithCode from '../LoginWithCode';

const Auth = ({ match, user }) => (
  <div>
    {user ?
      <Redirect to='/' /> :
      <Switch>
        <Route exact path={match.path} component={EnterPhoneNumber} />
        <Route path={`${match.path}/login-code`} component={LoginWithCode} />
      </Switch>
    }
  </div>
);

Auth.propTypes = {
  match: PropTypes.object.isRequired,
  user: PropTypes.object
};

export default Auth;
