import React from 'react';
import PropTypes from 'prop-types';
import { injectAsyncReducer } from '../../store';
import Component from './component';
import { auth, user } from './reducers';

class AuthIndex extends React.Component {
  static contextTypes = {
    store: PropTypes.object.isRequired
  };

  constructor(props, { store }) {
    super();

    injectAsyncReducer(store, 'auth', auth);
    injectAsyncReducer(store, 'user', user);
  }

  render() {
    return <Component {...this.props} />;
  }
}

export default AuthIndex;
