import React from 'react';
import PropTypes from 'prop-types';

const Dialog = ({ dialog }) => (
  <div>
    Dialog data
    <p>Top message: {dialog.top_message}</p>
    <p>User id: {dialog.peer.user_id}</p>
    <p> ---------------------------- </p>
  </div>
);

Dialog.propTypes = {
  dialog: PropTypes.object.isRequired
};

export default Dialog;
