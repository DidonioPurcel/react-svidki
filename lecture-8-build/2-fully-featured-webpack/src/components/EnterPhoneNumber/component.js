import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './styles.css';

class EnterPhoneNumber extends Component {
  state = {
    phone: ''
  };

  handleSubmit = (ev) => {
    ev.preventDefault();
    const { phone } = this.state;

    fetch(
      '/api/auth/send-code',
      {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ phone })
      }
    )
      .then(res => res.json())
      .then(data => this.props.handleAuthCodeData({
        phone,
        phoneCodeHash: data.phone_code_hash
      }))
      .then(() => {
        this.props.history.push(`${this.props.match.path}/login-code`);
      });
  };

  handlePhoneChange = (ev) => {
    this.setState({ phone: ev.target.value });
  };

  render() {
    return (
      <div className={styles.loginWrapper}>
        <h4 className={styles.loginHeader}>Sign in</h4>
        <p className={styles.loginDescription}>
          Please enter your full phone number linked with telegram account
        </p>

        <form className='clear' onSubmit={this.handleSubmit}>
          <label htmlFor='phone' className={styles.loginLabel}>
            Phone number
            <input
              id='phone'
              value={this.state.phone}
              onChange={this.handlePhoneChange}
              type='phone'
              name='phone'
              placeholder='380930000000'
            />
          </label>

          <button className={styles.loginSubmit} type='submit'>
            <h4>Next</h4>
          </button>
        </form>
      </div>
    );
  }
}

EnterPhoneNumber.propTypes = {
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  handleAuthCodeData: PropTypes.func.isRequired
};

export default EnterPhoneNumber;
