import { connect } from 'react-redux';
import LoginWithCodeComponent from './component';

const mapStateToProps = state => ({
  auth: state.auth
});

const mapDispatchToProps = dispatch => ({
  handleUserSignIn: (user) => {
    dispatch({
      type: 'SAVE_USER',
      user
    });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginWithCodeComponent);
