import React from 'react';

function loadAsync(componentName) {
  return class LoadAsync extends React.Component {
    state = { Component: null };

    componentWillMount() {
      if (this.state.Component !== null) { return null; }

      return import(`../${componentName}/index`)
        .then(m => m.default)
        .then(Component => this.setState({ Component }));
    }

    render() {
      const { Component } = this.state;
      if (!Component) { return null; }
      return <Component {...this.props} />;
    }
  };
}

export default loadAsync;
