/* eslint no-param-reassign: 0 */
import { createStore, combineReducers } from 'redux';
import { dialogs } from './components/Dialogs/reducers';

const userData = localStorage.getItem('user');

const defaultInitialState = {
  user: userData ? JSON.parse(userData) : null,
  auth: null,
  dialogs: null
};

export const createReducer = asyncReducers => combineReducers({
  dialogs,
  ...asyncReducers
});

export const injectAsyncReducer = (store, name, asyncReducer) => {
  store.asyncReducers[name] = asyncReducer;
  store.replaceReducer(createReducer(store.asyncReducers));
};

export default (initialState) => {
  const store = createStore(
    createReducer(),
    {
      ...defaultInitialState,
      ...initialState
    }
  );

  store.asyncReducers = {};

  store.subscribe(() => {
    localStorage.setItem('user', JSON.stringify(store.getState().user));
  });

  return store;
};
