Additional resources:
1) (Webpack org) https://webpack.js.org/
2) (Example of real webpack config) https://gist.github.com/kutyel/f0ff3cf03d453b7c1368a5ef35d0ea12
3) (What are CSS modules and why do we need them) https://css-tricks.com/css-modules-part-1-need/
4) (What are CSS modules. Plus webpack. Part 2) https://css-tricks.com/css-modules-part-2-getting-started/
5) (Widespread eslint config) https://www.npmjs.com/package/eslint-config-airbnb