import schema from './schema.graphqls';
import resolver from './resolver';

export default { schema, resolver };
