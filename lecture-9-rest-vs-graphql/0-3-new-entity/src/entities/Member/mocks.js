export default [
  { id: 1, name: 'Josh Lloyd-Watson' },
  { id: 2, name: 'Tom McFarland' },
  { id: 3, name: 'Fraser MacColl' },
  { id: 4, name: 'George Day' },
  { id: 5, name: 'Dominic Whalley' },
  { id: 6, name: 'Andro Cowperthwaite' },
  { id: 7, name: 'Rudi Salmon' },
  { id: 8, name: 'Dani Filth'},
  { id: 9, name: 'Martin "Marthus" Škaroupka'},
  { id: 10, name: 'Daniel Firth'},
  { id: 11, name: 'Lindsay Schoolcraft'},
  { id: 12, name: 'Richard Shaw'},
  { id: 13, name: 'Marek "Ashok" Šmerda'},
  { id: 14, name: 'Joe Newman'},
  { id: 15, name: 'Thom Sonny Green'},
  { id: 16, name: 'Gus Unger-Hamilton'}
];
