export default {
  Band: {
    id: root => root.id,
    name: root => root.name,
    origin: root => root.origin,
    years: root => root.years,
    genre: root => root.genre,
    members: root => root.members
  }
};
