import { createServer } from 'http';
import app from './app';

const port = 3000;
const server = createServer(app);
let currentApp = app;

server.listen(port, () => console.log(`GraphQL server is now running on port ${port}`));

if (module.hot) {
  module.hot.accept(['./app', './schema'], () => {
    server.removeListener('request', currentApp);
    server.on('request', app);
    currentApp = app;
  });
}
