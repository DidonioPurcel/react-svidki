import bands from '../Band/mocks';

export default {
  Album: {
    id: root => root.id,
    name: root => root.name,
    released: root => root.released,
    genre: root => root.genre,
    band: root => bands.find(band => band.id === root.band)
  }
};
