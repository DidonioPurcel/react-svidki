import bands from '../Band/mocks';
import albums from '../Album/mocks';

export default {
  Query: {
    bands: () => Promise.resolve(bands),
    band: (root, { id }) => Promise.resolve(bands.find(band => band.id === id)),
    albums: () => Promise.resolve(albums),
    album: (root, { id }) => Promise.resolve(albums.find(album => album.id === id)),
  }
};
