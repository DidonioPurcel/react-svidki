import { graphql } from 'react-apollo';
import AppComponent from './component';
import bandsQuery from './bandsQuery.graphql';

const mapGraphqlToProps = ({ data: { bands = [] } }) => ({ bands });

export default graphql(
  bandsQuery,
  { props: mapGraphqlToProps }
)(AppComponent);
