import express from 'express';
import { graphqlExpress, graphiqlExpress } from 'graphql-server-express';
import bodyParser from 'body-parser';
import cors from 'cors';

import schema from './schema';
import context from './context';
import bands from './entities/Band/mocks';

const db = require('monk')('localhost/bands');

const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

db.get('bands').find().then((bandsList) => {
  if (bandsList.length > 0) { return false; }
  bands.forEach(band => db.get('bands').insert(band));
  return true;
});

app.use('/graphql', graphqlExpress({ schema, context }));
app.use('/graphiql', graphiqlExpress({ endpointURL: '/graphql' }));

export default app;
