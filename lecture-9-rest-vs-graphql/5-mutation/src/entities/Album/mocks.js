export default [
  {
    id: 1,
    name: 'Jungle',
    released: '14 July 2014',
    genre: 'Neo-soul',
    band: 1
  },
  {
    id: 2,
    name: 'The Principle of Evil Made Flesh',
    released: '24 February 1994',
    genre: 'hybrid of gothic metal and black metal',
    band: 2
  },
  {
    id: 3,
    name: 'Godspeed on the Devi\'s Thunder',
    released: '28 October 2008',
    genre: 'extreme metal',
    band: 2
  },
  {
    id: 4,
    name: 'Hammer of the Witches',
    released: '10 July 2015',
    genre: 'extreme metal',
    band: 2
  },
  {
    id: 5,
    name: 'An Awesome Wave',
    released: '25 May 2012',
    genre: 'indie rock',
    band: 3
  },
  {
    id: 6,
    name: 'This Is All Yours',
    released: '22 September 2014',
    genre: 'indie rock',
    band: 3
  },
  {
    id: 7,
    name: 'Relaxer',
    released: '2 June 2017',
    genre: 'indie rock',
    band: 3
  },

];
