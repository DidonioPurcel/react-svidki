export default {
  addAlbum(_, { input }, context) {
    context.bands.push(input);
    return Promise.resolve(input);
  }
};
