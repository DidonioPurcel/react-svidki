export default {
  Album: {
    id: root => root.id,
    name: root => root.name,
    released: root => root.released,
    genre: root => root.genre,
    band: (root, args, context) => context.bands.find(band => band.id === root.band)
  }
};
