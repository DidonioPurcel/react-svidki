export default [
  {
    id: 1,
    name: 'Jungle',
    origin: 'London, England',
    years: '2013-present',
    genre: 'Neo-soul',
    members: [1, 2, 3, 4, 5, 6, 7],
    albums: [1]
  },
  {
    id: 2,
    name: 'Cradle of Filth',
    origin: 'Suffolk, England',
    years: '1991-present',
    genre: 'extreme metal',
    members: [8, 9, 10, 11, 12, 13],
    albums: [2, 3, 4]
  },
  {
    id: 3,
    name: 'Alt-J',
    origin: 'Leeds, England',
    years: '2007-present',
    genre: 'indie rock',
    members: [14, 15, 16],
    albums: [5, 6, 7]
  }
];
