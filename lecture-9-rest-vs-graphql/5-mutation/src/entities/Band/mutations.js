const db = require('monk')('localhost/bands');

export default {
  addBand(_, { input }) {
    return db.get('bands').insert(input);
  }
};
