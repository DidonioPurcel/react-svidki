import albums from '../Album/mocks';
import members from '../Member/mocks';

export default {
  Band: {
    id: root => root._id,
    name: root => root.name,
    origin: root => root.origin,
    years: root => root.years,
    genre: root => root.genre,
    members: root => root.members.map(id => members.find(member => member.id === id)),
    albums: root => albums.filter(album => album.band === root.id)
  }
};
