import albums from '../Album/mocks';
import bandMutations from '../Band/mutations';

const db = require('monk')('localhost/bands');

export default {
  Query: {
    bands: () => db.get('bands').find(),
    band: (root, { id }) => db.get('bands').find(id),
    albums: () => Promise.resolve(albums),
    album: (root, { id }) => Promise.resolve(albums.find(album => album.id === id)),
  },
  Mutation: {
    ...bandMutations
  }
};
