import { makeExecutableSchema } from 'graphql-tools';
import { merge } from 'lodash';

import Schema from './entities/Schema';
import Album from './entities/Album';
import Band from './entities/Band';
import Member from './entities/Member';

export default makeExecutableSchema({
  typeDefs: [
    Schema.schema,
    Album.schema,
    Band.schema,
    Member.schema
  ],
  resolvers: merge(
    Schema.resolver,
    Album.resolver,
    Band.resolver,
    Member.resolver
  )
});
