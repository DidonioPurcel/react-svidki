import { connect } from 'react-redux';
import EnterPhoneNumberComponent from './component';

const mapDispatchToProps = dispatch => ({
  handleAuthCodeData: auth => {
    dispatch({
      type: 'SAVE_AUTH_DATA',
      auth
    });
  }
});

export default connect(null, mapDispatchToProps)(EnterPhoneNumberComponent);
