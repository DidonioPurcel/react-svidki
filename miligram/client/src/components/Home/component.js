import React from 'react';
import Dialogs from '../Dialogs';
import './styles.css';

const Home = () => (
  <div className="home">
    <Dialogs />
  </div>
);

export default Home;
